# README #

UltraDMD has a rependancy on [XDMD](http://bitbucket.org/tspeirs/xdmd/).  Make sure that the two projects are in sibling folder.  That way the UltraDMD solution file can locate the XDMD project files.

If you do not wish to debug XDMD, then you can unload those projects from the solution and just build the UltraDMD component(s).  Just make sure you have the appropriate/latest XDMD.dll and XDMDNative.dll (see the download section).
