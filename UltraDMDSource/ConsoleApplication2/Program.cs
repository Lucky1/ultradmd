﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Runtime.InteropServices;
#endregion

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            XDMD.Device draw = new XDMD.Device(true, true);

            System.Diagnostics.Trace.WriteLine(System.IO.Directory.GetCurrentDirectory());
            draw.PlayFlash(System.IO.Directory.GetCurrentDirectory() + "\\uac.swf");

            XDMD.Surface SurfArcade = new XDMD.Surface(System.IO.Directory.GetCurrentDirectory() + "\\pinballx.png", draw);

            int TranLoop = -1;

            double ColDbl = 0.0;
            XDMD.Font f = new XDMD.Font(System.IO.Directory.GetCurrentDirectory() + "\\Fonts\\f7by13.gif", draw);
            //int x = 128;
            int ColDir = 0;

            int score = 0;
            XDMD.Surface SurfVideo = null;
            while (true)
            {
                if (Console.KeyAvailable)
                {
                    // Read the key and display it (false to hide it)
                    ConsoleKeyInfo key = Console.ReadKey(true);
                    if (ConsoleKey.Escape == key.Key)
                    {
                        break;
                    }
                }

                if (ColDir == 0)
                {
                    ColDbl += 1.0;
                    if ((int)(ColDbl) > 15)
                    {
                        ColDir = 1;
                        ColDbl = 15.0;
                    }
                }
                else
                {
                    ColDbl -= 1.0;
                    if ((int)(ColDbl) < 1)
                    {
                        ColDbl = 0.0;
                        ColDir = 0;
                    }
                }
                if (SurfVideo != null)
                {
                    draw.DrawVideoFrame(SurfVideo, new Rectangle(0, 0, SurfVideo.Width, SurfVideo.Height));
                }

                if (draw.SurfaceFontTransitionState == XDMD.Device.PlayState.Ready)
                {
                    ++TranLoop;

                    switch (TranLoop)
                    {
                        case 0:
                            draw.TransitionSurface(SurfArcade, new Rectangle(0, 0, 128, 32), XDMD.Device.AnimationType.FadeIn, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_02, 120, XDMD.Device.AnimationType.FadeOut, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_03);
                            //draw.TransitionSurface(SurfArcade, new Rectangle(0, 0, 128, 32), XDMD.Device.AnimationType.FadeIn, XDMD.Device.AnimationType.ScrollOnDown, XDMD.Device.AnimationSpeed.Speed_02, 120, XDMD.Device.AnimationType.ZoomOut, XDMD.Device.AnimationType.FadeOut, XDMD.Device.AnimationSpeed.Speed_03);
                            break;
                        case 1:
                            draw.TransitionFont(f, "Welcome to the XDMD Demo.", 10, 10, XDMD.Device.AnimationType.ScrollOnLeft, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_02, 0, XDMD.Device.AnimationType.ScrollOffLeft, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_02);
                            break;
                        case 2:
                            draw.PlayVideo(System.IO.Directory.GetCurrentDirectory() + "\\Wildlife.wmv", true, false, new Rectangle(0, -10, 128, 62), false);
                            SurfVideo = new XDMD.Surface(128, (int)((128 / draw.VideoWidth) * (draw.VideoHeight)), draw);
                            draw.TransitionSurface(SurfVideo
                                               , new Rectangle(0, 0, 128, SurfVideo.Height)
                                               , XDMD.Device.AnimationType.ScrollOnUp
                                               , XDMD.Device.AnimationType.None
                                               , XDMD.Device.AnimationSpeed.Speed_02
                                               , 0
                                               , XDMD.Device.AnimationType.ScrollOffUp
                                               , XDMD.Device.AnimationType.None
                                               , XDMD.Device.AnimationSpeed.Speed_02);
                            break;
                        case 3:
                            draw.TransitionSurface(SurfVideo, new Rectangle(0, 0, 128, SurfVideo.Height), XDMD.Device.AnimationType.ScrollOnDown, XDMD.Device.AnimationType.ScrollOnLeft, XDMD.Device.AnimationSpeed.Speed_02, 0, XDMD.Device.AnimationType.FadeOut, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_02);
                            break;
                        case 4:
                            draw.StopVideo();
                            SurfVideo.Dispose();
                            SurfVideo = null;
                            break;
                        case 5:
                            draw.TransitionFont(f, "Text Fade.", 10, 10, XDMD.Device.AnimationType.FadeIn, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_02, 0, XDMD.Device.AnimationType.FadeOut, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_02);
                            break;
                        case 6:
                            draw.TransitionFont(f, "Zoom.", -18, 0, 32, 32, XDMD.Device.AnimationType.ZoomIn, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_02, 0, XDMD.Device.AnimationType.ZoomOut, XDMD.Device.AnimationType.FadeOut, XDMD.Device.AnimationSpeed.Speed_02);
                            break;
                        case 7:
                            ++score;
                            f.Draw(10, 2, 15, "Score: " + String.Format("{0,8}", score));
                            f.Draw(10, 18, 15, "Score: " + String.Format("{0,8}", score + 12345));
                            if (score < 500)
                            {
                                --TranLoop;
                            }
                            else
                            {
                                ColDbl = 0.0;
                                ColDir = 0;
                                score = 0;
                            }
                            break;
                        case 8:
                            {
                                //int c = 15;
                                ++score;
                                f.Draw(50, 10, (int)(ColDbl), "XDMD");
                                if (score < 300)
                                {
                                    --TranLoop;
                                }
                                else
                                {
                                    score = 0;
                                }
                            }
                            break;
                        default:
                            score = 0;
                            TranLoop = -1;
                            break;
                    }
                }

                draw.RenderWait();
            }
            draw.Dispose();
            System.Threading.Thread.Sleep(4000);
        }
    }
}
