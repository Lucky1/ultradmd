﻿#region Using directives
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.ComponentModel;
#endregion


namespace UltraDMD
{
    public class DisplaySceneScrollingLine : DisplaySceneSingleLine
    {
        protected Int32 _textOutlineBrightness;
        protected XDMD.Font _fontNormalOutline;
        protected double _xText;

        public DisplaySceneScrollingLine(string background, string text, Int32 textBrightness, Int32 textOutlineBrightness
            , XDMD.Device.AnimationType animateIn, Int32 pauseTime, XDMD.Device.AnimationType animateOut, Int32 videoStretchMode) :
            base(background, text, textBrightness, animateIn, pauseTime, animateOut, videoStretchMode)
        {
            _textOutlineBrightness = textOutlineBrightness;
            _fontNormalOutline = null;
            _xText = 128.0;
        }
        protected void DrawContent_NoOutline()
        {
            if (_fontNormal == null)
            {
                //string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                //_fontNormal = new XDMD.Font(exePath + "\\Fonts\\f14by26.gif", Program.draw);
                _fontNormal = new XDMD.Font(Program.GetImageByName("f14by26"), Program.draw);
                _sizeTop = _fontNormal.MeasureString(_text);
            }
            _fontNormal.Draw(_surfaceContent, (int)_xText, (32 - _sizeTop.Height) / 2, (_textBrightness < 0 ? 15 : (_textBrightness % 16)), _text, Program.color);
        }
        protected void DrawContent_NoFill()
        {
            if (_fontNormalOutline == null)
            {
                //string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                //_fontNormalOutline = new XDMD.Font(exePath + "\\Fonts\\f14by26.outline.gif", Program.draw);
                _fontNormalOutline = new XDMD.Font(Program.GetImageByName("f14by26_outline"), Program.draw);
                _sizeTop = _fontNormalOutline.MeasureString(_text);
            }
            _fontNormalOutline.Draw(_surfaceContent, (int)_xText, (32 - _sizeTop.Height) / 2, (_textOutlineBrightness < 0 ? 15 : (_textOutlineBrightness % 16)), _text, Program.color);
        }
        protected void DrawContent_OutlineAndFill()
        {
            if (_fontNormal == null)
            {
                //string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                //_fontNormal = new XDMD.Font(exePath + "\\Fonts\\f14by26.fill.gif", Program.draw);
                _fontNormal = new XDMD.Font(Program.GetImageByName("f14by26_fill"), Program.draw);
                //_fontNormalOutline = new XDMD.Font(exePath + "\\Fonts\\f14by26.outline.gif", Program.draw);
                _fontNormalOutline = new XDMD.Font(Program.GetImageByName("f14by26_outline"), Program.draw);
                _sizeTop = _fontNormalOutline.MeasureString(_text);
            }
            _fontNormalOutline.Draw(_surfaceContent, (int)_xText, (32 - _sizeTop.Height) / 2, (_textOutlineBrightness % 16), _text, Program.color);
            _fontNormal.Draw(_surfaceContent, (int)_xText, (32 - _sizeTop.Height) / 2, (_textBrightness % 16), _text, Program.color);
        }

        protected override void DrawContent()
        {
            _surfaceContent.Clear();
            if (_textOutlineBrightness < 0)
            {   //-1 indicates NO outline
                DrawContent_NoOutline();
            }
            else if (_textBrightness < 0)
            {   //-1 indicates DON'T fill (outline only - we'll ignore the error case of BOTH values being -1)
                DrawContent_NoFill();
            }
            else
            {   //outline AND fill
                DrawContent_OutlineAndFill();
            }
            _xText = _xText - 1.2;
        }
    }
}
