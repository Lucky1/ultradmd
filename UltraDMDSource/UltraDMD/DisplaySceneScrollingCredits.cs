﻿#region Using directives
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.ComponentModel;
#endregion

namespace UltraDMD
{
    public class DisplaySceneScrollingCredits : DisplayScene
    {
        protected Object _backgroundObj;
        protected string _background;
        protected string[] _text;
        protected Int32 _pauseTime;
        protected XDMD.Device.AnimationType _animateIn;
        protected XDMD.Device.AnimationType _animateOut;

        protected Int32 _step;
        protected XDMD.Surface _surface;
        protected XDMD.Surface _surfaceContent;
        protected bool _bVideoSurface;

        protected XDMD.Font _fontNormal;
        protected Int32 _textBrightness;
        protected Size[] _sizeTop;
        protected Int32 _endTime;
        protected Int32 _videoStretchMode;
        protected System.Collections.ArrayList _listImages;

        public DisplaySceneScrollingCredits(string background, string[] text, Int32 textBrightness, XDMD.Device.AnimationType animateIn, Int32 pauseTime, XDMD.Device.AnimationType animateOut, Int32 videoStretchMode)
        {
            _backgroundObj = null;
            _background = background;
            _animateIn = animateIn;
            _animateOut = animateOut;
            _pauseTime = pauseTime;
            _step = 0;
            _text = text;

            _surface = null;
            _bVideoSurface = false;
            _endTime = 0;
            _fontNormal = null;
            _videoStretchMode = videoStretchMode;
            _textBrightness = textBrightness;
        }

        public override void SetBackgroundObj(ref Object obj)
        {
            _backgroundObj = obj;
        }

        public override bool IsAnimated()
        {
            return _bVideoSurface || (_backgroundObj != null);
        }

        protected virtual void DrawContent()
        {
            if (_fontNormal == null)
            {
                _sizeTop = new Size[_text.Length];

                //string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                //_fontNormal = new XDMD.Font(exePath + "\\Fonts\\f5by7.gif", Program.draw);
                _fontNormal = new XDMD.Font(Program.GetImageByName("f5by7"), Program.draw);
                for (int i = 0; i < _text.Length; ++i)
                {
                    _sizeTop[i] = _fontNormal.MeasureString(_text[i]);
                }
                if (_bVideoSurface)
                {
                    for (int i = 0; i < _text.Length; ++i)
                    {
                        _fontNormal.Draw(_surfaceContent, (128 - _sizeTop[i].Width) / 2, 32 + (10 * i), _textBrightness, _text[i], Program.color);
                    }
                }
                else
                {
                    for (int i = 0; i < _text.Length; ++i)
                    {
                        _fontNormal.Draw(_surfaceContent, (128 - _sizeTop[i].Width) / 2, 32 + (10 * i), _textBrightness, _text[i], Program.color);
                    }
                }
            }

            if (_bVideoSurface)
            {
                _surfaceContent.draw(0, 0);
            }
            else
            {
                _surfaceContent.draw(0, 0);
            }
        }

        public override void DrawVideoFrame()
        {
            /*
            if (_bVideoSurface && _surface != null)
            {
                Program.draw.DrawVideoFrame(_surface, new Rectangle(0, 0, _surface.Width, _surface.Height));
                DrawContent();
            }
            else if (_animationImageList != null && _surface != null)
            {
                ++_step;
                XDMD.Surface s = (XDMD.Surface)(_listImages[_step % _listImages.Count]);
                s.draw(0, 0);
                DrawContent();
            }*/
        }

        public override void StopVideo()
        {
            if (_bVideoSurface && _surface != null)
            {
                Program.draw.StopVideo();
            }
        }

        public override bool Show()
        {
            if (_step > 0)
            {
                if (System.Environment.TickCount > _endTime)
                {
                    return true;
                }

                return false;
            }
            ++_step;

            try
            {
                bool bError = false;
                if (_background != null && _background.Length > 0 && !System.IO.File.Exists(_background))
                {
                    using (System.IO.StreamWriter sw = System.IO.File.AppendText(System.IO.Path.Combine(System.IO.Path.GetTempPath(), "UltraDMD.log")))
                    {
                        sw.WriteLine(DateTime.Now + " file not found: " + _background);
                    } 
                    _background = null;
                    bError = true;
                }

                if (_background != null && _background.Length > 0)
                {
                    if (System.IO.Path.GetExtension(_background).ToLower().Equals(".wmv") || System.IO.Path.GetExtension(_background).ToLower().Equals(".mp4"))
                    {
                        Bitmap bmpBkgnd = Program.GetImageByName("border00");
                        _surface = new XDMD.Surface(bmpBkgnd, Program.draw);
                        //_bVideoSurface = true;
                        //Program.draw.PlayVideo(_background, true, false, new Rectangle(0, 0, 128, 32), _videoStretchMode > 0);
                        //_surface = new XDMD.Surface(128, (int)((128.0 / Program.draw.VideoWidth) * (Program.draw.VideoHeight)), Program.draw);
                    }
                    else
                    {
                        _surface = new XDMD.Surface(_background, Program.draw);
                    }
                }
                    /*
                else if (_animationImageList != null)
                {
                    _surface = new XDMD.Surface(128, 32, Program.draw);
                    _listImages = new System.Collections.ArrayList();

                    foreach (string strImg in _animationImageList._aImages)
                    {
                        _listImages.Add(new XDMD.Surface(strImg, Program.draw));
                    }
                }*/
                else
                {
                    Bitmap bmpBkgnd = Program.GetImageByName(bError ? "error00" : "border00");
                    _surface = new XDMD.Surface(bmpBkgnd, Program.draw);
                }
                int hCredits = 32 + (10 * (_text.Length+1));
                _surfaceContent = new XDMD.Surface(128, hCredits, Program.draw);

                _endTime = System.Environment.TickCount + _pauseTime;

                Program.draw.Clear();
                DrawContent();

                if (_animateIn != XDMD.Device.AnimationType.None || _animateOut != XDMD.Device.AnimationType.None)
                {
                    Int32 h = (_videoStretchMode == 0 ? 32 : _surface.Height);
                    Int32 y = 0;
                    if (_videoStretchMode == 2)
                    {
                        y = (32 - h) / 2;
                    }
                    else if (_videoStretchMode == 3)
                    {
                        y = 32 - h;
                    }
                    Program.draw.TransitionSurface(_surface, new Rectangle(0, y, 128, h), _animateIn, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_24, (int)(hCredits * 3), _animateOut, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_08);
                    if (_animateIn == XDMD.Device.AnimationType.ScrollOnUp)
                    {
                        Program.draw.TransitionSurface(_surfaceContent, new Rectangle(0, -1 * hCredits, 128, hCredits), _animateIn, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_01, _pauseTime * 10 / (24 * 7), XDMD.Device.AnimationType.None, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_08);
                    }
                    else if(_animateIn == XDMD.Device.AnimationType.ScrollOnDown)
                    {
                        Program.draw.TransitionSurface(_surfaceContent, new Rectangle(0, 22, 128, hCredits), _animateIn, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_01, _pauseTime * 10 / (24 * 7), XDMD.Device.AnimationType.None, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_08);
                    }
                }
                else
                {
                    _surface.draw(0, 0);
                }
            }
            catch (Exception ex)
            {
                throw ex; // Re-throw the exception
            }
            return false;
        }
    }
}
