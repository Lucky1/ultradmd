﻿#region Using directives
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.ComponentModel;
#endregion

namespace UltraDMD
{
    public class DisplaySceneOutlineTwoLines : DisplaySceneTwoLines
    {
        protected Int32 _textOutlineBrightness;
        protected XDMD.Font _fontNormalOutline;

        protected Int32 _bottomOutlineBrightness;
        protected XDMD.Font _fontBottomOutline;

        public DisplaySceneOutlineTwoLines(string background, string toptext, Int32 topBrightness, Int32 topOutlineBrightness, string bottomtext, Int32 bottomBrightness, Int32 bottomOutlineBrightness, XDMD.Device.AnimationType animateIn, Int32 pauseTime, XDMD.Device.AnimationType animateOut, Int32 videoStretchMode) :
            base(background, toptext, topBrightness, bottomtext, bottomBrightness, animateIn, pauseTime, animateOut, videoStretchMode)
        {
            _textOutlineBrightness = topOutlineBrightness;
            _fontNormalOutline = null;

            _bottomOutlineBrightness = bottomOutlineBrightness;
            _fontBottomOutline = null;
        }

        protected virtual void DrawContent_TopNoFill()
        {
            if (_fontNormalOutline == null)
            {
                //string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                //_fontNormalOutline = new XDMD.Font(exePath + "\\Fonts\\f5by7.outline.gif", Program.draw);
                _fontNormalOutline = new XDMD.Font(Program.GetImageByName("f5by7_outline"), Program.draw);
                _sizeTop = _fontNormalOutline.MeasureString(_text);
            }
            _fontNormalOutline.Draw(_surfaceContent, (128 - _sizeTop.Width) / 2, 4, (_textOutlineBrightness < 0 ? 15 : (_textOutlineBrightness % 16)), _text, Program.color);
        }

        protected virtual void DrawContent_TopNoOutline()
        {
            if (_fontNormal == null)
            {
                //string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                //_fontNormal = new XDMD.Font(exePath + "\\Fonts\\f5by7.fill.gif", Program.draw);
                _fontNormal = new XDMD.Font(Program.GetImageByName("f5by7_fill"), Program.draw);
                _sizeTop = _fontNormal.MeasureString(_text);
            }
            _fontNormal.Draw(_surfaceContent, (128 - _sizeTop.Width) / 2, 4, (_textBrightness < 0 ? 15 : (_textBrightness % 16)), _text, Program.color);
        }

        protected virtual void DrawContent_TopOutlineAndFill()
        {
            if (_fontNormal == null)
            {
                //string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                //_fontNormal = new XDMD.Font(exePath + "\\Fonts\\f5by7.fill.gif", Program.draw);
                _fontNormal = new XDMD.Font(Program.GetImageByName("f5by7_fill"), Program.draw);
                //_fontNormalOutline = new XDMD.Font(exePath + "\\Fonts\\f5by7.outline.gif", Program.draw);
                _fontNormalOutline = new XDMD.Font(Program.GetImageByName("f5by7_outline"), Program.draw);
                _sizeTop = _fontNormalOutline.MeasureString(_text);
            }
            _fontNormalOutline.Draw(_surfaceContent, (128 - _sizeTop.Width) / 2, 4, (_textOutlineBrightness < 0 ? 15 : (_textOutlineBrightness % 16)), _text, Program.color);
            _fontNormal.Draw(_surfaceContent, (128 - _sizeTop.Width) / 2, 4, (_textBrightness < 0 ? 15 : (_textBrightness % 16)), _text, Program.color);
        }

        protected virtual void DrawContent_BottomNoFill()
        {
            if (_fontBottomOutline == null)
            {
                //string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                //_fontBottomOutline = new XDMD.Font(exePath + "\\Fonts\\f6by12.outline.gif", Program.draw);
                _fontBottomOutline = new XDMD.Font(Program.GetImageByName("f6by12_outline"), Program.draw);
                _sizeBottom = _fontBottomOutline.MeasureString(_bottomtext);
            }
            _fontBottomOutline.Draw(_surfaceContent, (128 - _sizeBottom.Width) / 2, 15, (_bottomOutlineBrightness < 0 ? 15 : (_bottomOutlineBrightness % 16)), _bottomtext, Program.color);
        }

        protected virtual void DrawContent_BottomNoOutline()
        {
            if (_fontBottom == null)
            {
                //string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                //_fontBottom = new XDMD.Font(exePath + "\\Fonts\\f6by12.fill.gif", Program.draw);
                _fontBottom = new XDMD.Font(Program.GetImageByName("f6by12_fill"), Program.draw);
                _sizeBottom = _fontBottom.MeasureString(_bottomtext);
            }
            _fontBottom.Draw(_surfaceContent, (128 - _sizeBottom.Width) / 2, 15, (_bottomBrightness < 0 ? 15 : (_bottomBrightness % 16)), _bottomtext, Program.color);
        }

        protected virtual void DrawContent_BottomOutlineAndFill()
        {
            if (_fontBottom == null)
            {
                //string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                //_fontBottom = new XDMD.Font(exePath + "\\Fonts\\f6by12.fill.gif", Program.draw);
                _fontBottom = new XDMD.Font(Program.GetImageByName("f6by12_fill"), Program.draw);
                //_fontBottomOutline = new XDMD.Font(exePath + "\\Fonts\\f6by12.outline.gif", Program.draw);
                _fontBottomOutline = new XDMD.Font(Program.GetImageByName("f6by12_outline"), Program.draw);
                _sizeBottom = _fontBottomOutline.MeasureString(_bottomtext);
            }
            _fontBottomOutline.Draw(_surfaceContent, (128 - _sizeBottom.Width) / 2, 15, (_bottomOutlineBrightness < 0 ? 15 : (_bottomOutlineBrightness % 16)), _bottomtext, Program.color);
            _fontBottom.Draw(_surfaceContent, (128 - _sizeBottom.Width) / 2, 15, (_bottomBrightness < 0 ? 15 : (_bottomBrightness % 16)), _bottomtext, Program.color);
        }

        protected override void DrawContent()
        {
            if (_textOutlineBrightness < 0)
            {   //-1 indicates NO outline
                DrawContent_TopNoOutline();
            }
            else if (_textBrightness < 0)
            {   //-1 indicates DON'T fill (outline only - we'll ignore the error case of BOTH values being -1)
                DrawContent_TopNoFill();
            }
            else
            {   //outline AND fill
                DrawContent_TopOutlineAndFill();
            }

            if (_bottomOutlineBrightness < 0)
            {   //-1 indicates NO outline
                DrawContent_BottomNoOutline();
            }
            else if (_bottomBrightness < 0)
            {   //-1 indicates DON'T fill (outline only - we'll ignore the error case of BOTH values being -1)
                DrawContent_BottomNoFill();
            }
            else
            {   //outline AND fill
                DrawContent_BottomOutlineAndFill();
            }

            _surfaceContent.draw(_surface, 0, 0);
        }
    }
}
