﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UltraDMD
{
    public partial class FormConfigureUltraDMD : Form
    {
        private List<string> colors;
        private List<string> sizeDMD;
        private Color colorPrev;
        private Boolean flipYPrev;
        private string strColorNew;
        private Int32 height;

        public Int32 GetMajorVersion()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            return fvi.FileMajorPart;
        }

        public Int32 GetMinorVersion()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            return fvi.FileMinorPart;
        }

        public Int32 GetBuildNumber()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            return fvi.FileBuildPart * 10000 + fvi.FilePrivatePart;
        }

        public FormConfigureUltraDMD()
        {
            InitializeComponent();
            sizeDMD = null;
            colors = null;
            strColorNew = null;

            groupBox2.Text = string.Format("UltraDMD version {0}.{1}.{2}",  GetMajorVersion(), GetMinorVersion(), GetBuildNumber());
            label2.Text = "Copyright 2013-2015 Stephen Rakonza";
            refreshDMD();
        }

        private void linkLabelXDMD_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://xdmd.info");
        }

        private void linkLabelSpesoft_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.spesoft.com");
        }

        private void linkLabelUltraDMD_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://ultradmd.wordpress.com");
        }
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void addColor(string s, ref string sel)
        {
            colors.Add(s);
            if (colorPrev.Equals(Color.FromName(s)))
            {
                sel = s;
            }
        }

        private void refreshDMD()
        {
            Bitmap bmpBkgnd = Program.GetImageByName("border00");

            XDMD.Surface s = new XDMD.Surface(bmpBkgnd, Program.draw);
            XDMD.Font fontNormal = new XDMD.Font(Program.GetImageByName("f5by7"), Program.draw);
            XDMD.Font fontHighlight = new XDMD.Font(Program.GetImageByName("f6by12"), Program.draw);

            string _toptext = "BONUS";
            string _bottomtext = "2 MILLION";
            Size sizeTop = fontNormal.MeasureString(_toptext);
            fontNormal.Draw(s, (128 - sizeTop.Width) / 2, 5, 13, _toptext);

            Size sizeBottom = fontHighlight.MeasureString(_bottomtext);
            fontHighlight.Draw(s, (128 - sizeBottom.Width) / 2, 16, 15, _bottomtext);

            Program.draw.Clear();
            s.draw(0, 0);
            Program.draw.Render();
        }

        private void FormConfigureUltraDMD_Load(object sender, EventArgs e)
        {
            colorPrev = Program.draw.Color;
            flipYPrev = Program.draw.FlipY;

            colors = new List<string>();
            sizeDMD = new List<string>();

            int height = 32;
            sizeDMD.Add(string.Format("{0} x {1}", height * 4, height)); height += 32;
            sizeDMD.Add(string.Format("{0} x {1}", height * 4, height)); height += 32;
            sizeDMD.Add(string.Format("{0} x {1}", height * 4, height)); height += 32;
            sizeDMD.Add(string.Format("{0} x {1}", height * 4, height)); height += 32;
            sizeDMD.Add(string.Format("{0} x {1}", height * 4, height)); height += 32;
            sizeDMD.Add(string.Format("{0} x {1}", height * 4, height)); height += 32;
            sizeDMD.Add(string.Format("{0} x {1}", height * 4, height)); height += 32;
            sizeDMD.Add(string.Format("{0} x {1}", height * 4, height)); height += 32;
            sizeDMD.Add(string.Format("{0} x {1}", height * 4, height)); height += 32;
            sizeDMD.Add(string.Format("{0} x {1}", height * 4, height)); height += 32;
            sizeDMD.Add(string.Format("{0} x {1}", height * 4, height)); height += 32;
            sizeDMD.Add(string.Format("{0} x {1}", height * 4, height)); height += 32;
            sizeDMD.Add(string.Format("{0} x {1}", height * 4, height)); height += 32;
            sizeDMD.Add(string.Format("{0} x {1}", height * 4, height)); height += 32;
            sizeDMD.Add(string.Format("{0} x {1}", height * 4, height)); height += 32;

            listBoxDMDSize.DataSource = sizeDMD;
            listBoxDMDSize.SelectedIndex = sizeDMD.IndexOf(string.Format("{0} x {1}", Program.hDMD * 4, Program.hDMD));

            string selColor = "";
            addColor("AliceBlue", ref selColor);
            addColor("AliceBlue", ref selColor);
            addColor("AntiqueWhite", ref selColor);
            addColor("Aqua", ref selColor);
            addColor("Aquamarine", ref selColor);
            addColor("Azure", ref selColor);
            addColor("Beige", ref selColor);
            addColor("Bisque", ref selColor);
            addColor("BlanchedAlmond", ref selColor);
            addColor("Blue", ref selColor);
            addColor("BlueViolet", ref selColor);
            addColor("Brown", ref selColor);
            addColor("BurlyWood", ref selColor);
            addColor("CadetBlue", ref selColor);
            addColor("Chartreuse", ref selColor);
            addColor("Chocolate", ref selColor);
            addColor("Coral", ref selColor);
            addColor("CornflowerBlue", ref selColor);
            addColor("Cornsilk", ref selColor);
            addColor("Crimson", ref selColor);
            addColor("Cyan", ref selColor);
            addColor("DarkBlue", ref selColor);
            addColor("DarkCyan", ref selColor);
            addColor("DarkGoldenrod", ref selColor);
            addColor("DarkGray", ref selColor);
            addColor("DarkGreen", ref selColor);
            addColor("DarkKhaki", ref selColor);
            addColor("DarkMagenta", ref selColor);
            addColor("DarkOliveGreen", ref selColor);
            addColor("DarkOrange", ref selColor);
            addColor("DarkOrchid", ref selColor);
            addColor("DarkRed", ref selColor);
            addColor("DarkSalmon", ref selColor);
            addColor("DarkSeaGreen", ref selColor);
            addColor("DarkSlateBlue", ref selColor);
            addColor("DarkSlateGray", ref selColor);
            addColor("DarkTurquoise", ref selColor);
            addColor("DarkViolet", ref selColor);
            addColor("DeepPink", ref selColor);
            addColor("DeepSkyBlue", ref selColor);
            addColor("DimGray", ref selColor);
            addColor("DodgerBlue", ref selColor);
            addColor("Firebrick", ref selColor);
            addColor("FloralWhite", ref selColor);
            addColor("ForestGreen", ref selColor);
            addColor("Fuchsia", ref selColor);
            addColor("Gainsboro", ref selColor);
            addColor("GhostWhite", ref selColor);
            addColor("Gold", ref selColor);
            addColor("Goldenrod", ref selColor);
            addColor("Gray", ref selColor);
            addColor("Green", ref selColor);
            addColor("GreenYellow", ref selColor);
            addColor("Honeydew", ref selColor);
            addColor("HotPink", ref selColor);
            addColor("IndianRed", ref selColor);
            addColor("Indigo", ref selColor);
            addColor("Ivory", ref selColor);
            addColor("Khaki", ref selColor);
            addColor("Lavender", ref selColor);
            addColor("LavenderBlush", ref selColor);
            addColor("LawnGreen", ref selColor);
            addColor("LemonChiffon", ref selColor);
            addColor("LightBlue", ref selColor);
            addColor("LightCoral", ref selColor);
            addColor("LightCyan", ref selColor);
            addColor("LightGoldenrodYellow", ref selColor);
            addColor("LightGray", ref selColor);
            addColor("LightGreen", ref selColor);
            addColor("LightPink", ref selColor);
            addColor("LightSalmon", ref selColor);
            addColor("LightSeaGreen", ref selColor);
            addColor("LightSkyBlue", ref selColor);
            addColor("LightSlateGray", ref selColor);
            addColor("LightSteelBlue", ref selColor);
            addColor("LightYellow", ref selColor);
            addColor("Lime", ref selColor);
            addColor("LimeGreen", ref selColor);
            addColor("Linen", ref selColor);
            addColor("Magenta", ref selColor);
            addColor("Maroon", ref selColor);
            addColor("MediumAquamarine", ref selColor);
            addColor("MediumBlue", ref selColor);
            addColor("MediumOrchid", ref selColor);
            addColor("MediumPurple", ref selColor);
            addColor("MediumSeaGreen", ref selColor);
            addColor("MediumSlateBlue", ref selColor);
            addColor("MediumSpringGreen", ref selColor);
            addColor("MediumTurquoise", ref selColor);
            addColor("MediumVioletRed", ref selColor);
            addColor("MidnightBlue", ref selColor);
            addColor("MintCream", ref selColor);
            addColor("MistyRose", ref selColor);
            addColor("Moccasin", ref selColor);
            addColor("NavajoWhite", ref selColor);
            addColor("Navy", ref selColor);
            addColor("OldLace", ref selColor);
            addColor("Olive", ref selColor);
            addColor("OliveDrab", ref selColor);
            addColor("Orange", ref selColor);
            addColor("OrangeRed", ref selColor);
            addColor("Orchid", ref selColor);
            addColor("PaleGoldenrod", ref selColor);
            addColor("PaleGreen", ref selColor);
            addColor("PaleTurquoise", ref selColor);
            addColor("PaleVioletRed", ref selColor);
            addColor("PapayaWhip", ref selColor);
            addColor("PeachPuff", ref selColor);
            addColor("Peru", ref selColor);
            addColor("Pink", ref selColor);
            addColor("Plum", ref selColor);
            addColor("PowderBlue", ref selColor);
            addColor("Purple", ref selColor);
            addColor("Red", ref selColor);
            addColor("RosyBrown", ref selColor);
            addColor("RoyalBlue", ref selColor);
            addColor("SaddleBrown", ref selColor);
            addColor("Salmon", ref selColor);
            addColor("SandyBrown", ref selColor);
            addColor("SeaGreen", ref selColor);
            addColor("SeaShell", ref selColor);
            addColor("Sienna", ref selColor);
            addColor("Silver", ref selColor);
            addColor("SkyBlue", ref selColor);
            addColor("SlateBlue", ref selColor);
            addColor("SlateGray", ref selColor);
            addColor("Snow", ref selColor);
            addColor("SpringGreen", ref selColor);
            addColor("SteelBlue", ref selColor);
            addColor("Tan", ref selColor);
            addColor("Teal", ref selColor);
            addColor("Thistle", ref selColor);
            addColor("Tomato", ref selColor);
            addColor("Transparent", ref selColor);
            addColor("Turquoise", ref selColor);
            addColor("Violet", ref selColor);
            addColor("Wheat", ref selColor);
            addColor("White", ref selColor);
            addColor("WhiteSmoke", ref selColor);
            addColor("Yellow", ref selColor);
            addColor("YellowGreen", ref selColor);

            listBoxDMDColor.DataSource = colors;

            listBoxDMDColor.SelectedIndex = colors.IndexOf(selColor);
            checkBox2.Checked = flipYPrev;
            checkBoxFullColor.Checked = Program.bFullColor;
            refreshDMD();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!Program.draw.Color.Equals(colorPrev))
            {
                Program.draw.Color = colorPrev;
                refreshDMD();
            }
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;

            if (height != Program.hDMD)
            {
                MessageBox.Show("New DMD size will be applied on next instance.", "",
                                     MessageBoxButtons.OK,
                                     MessageBoxIcon.Information);
            }

            Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\UltraDMD", true);
            if (key != null)
            {
                if (height != Program.hDMD)
                {
                    key.SetValue("w", height * 4);
                    key.SetValue("h", height);
                }
                if (!Program.draw.Color.Equals(colorPrev) && strColorNew != null)
                {
                    key.SetValue("color", strColorNew);
                }
                key.SetValue("fullColor", checkBoxFullColor.Checked);
                key.SetValue("hideVDMD", checkBoxHideVDMD.Checked);
                key.SetValue("flipY", checkBox2.Checked);
            }
            if (checkBoxHideVDMD.Checked)
            {
                Program.draw.Visible = false;
            }
            Program.draw.FlipY = checkBox2.Checked;
            Program.draw.Clear();
            Program.draw.Render();

            this.Close();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            buttonOK_Click(null, null);
            Application.Exit();
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        private void listBoxDMDColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            strColorNew = colors[listBoxDMDColor.SelectedIndex];
            Color c = Color.FromName(strColorNew);
            Program.draw.Color = c;
            refreshDMD();
        }

        private void listBoxDMDSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sizeDMD != null)
            {
                string dimension = sizeDMD[listBoxDMDSize.SelectedIndex];
                string [] split = dimension.Split(new Char [] {' ', 'x'});

                Int32 w = Convert.ToInt32(split[0]);
                height = Convert.ToInt32(split[3]);
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            MessageBox.Show("Feature not yet implemented", "",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Information);
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void checkBoxHideVDMD_CheckedChanged(object sender, EventArgs e)
        {
            listBoxDMDSize.Enabled = !(checkBoxHideVDMD.Checked);
        }

        private void buttonDisplayErrorLog_Click(object sender, EventArgs e)
        {
            string filename = System.IO.Path.Combine(System.IO.Path.GetTempPath(), "UltraDMD.log");
            System.Diagnostics.Process.Start("notepad.exe", filename);
        }

        private void buttonClearErrorLog_Click(object sender, EventArgs e)
        {
            string filename = System.IO.Path.Combine(System.IO.Path.GetTempPath(), "UltraDMD.log");
            System.IO.FileStream fileStream = new System.IO.FileStream(filename, System.IO.FileMode.Truncate);
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
