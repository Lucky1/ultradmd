﻿/****************************** Module Header ******************************\
* Module Name:	DMDObject.cs
* Project:		UltraDMD
* Copyright (c) Microsoft Corporation.
* 
* The definition of the COM class, DMDObject, and its ClassFactory, 
* DMDObjectClassFactory.
* 
* (Please generate new GUIDs when you are writing your own COM server) 
* Program ID: UltraDMD.DMDObject
* CLSID_DMDObject: E1612654-304A-4E07-A236-EB64D6D4F511
* IID_IDMDObject: F7E68187-251F-4DFB-AF79-F1D4D69EE188
* DIID_IDMDObjectEvents: 0DECFF48-5492-43E7-AB6C-BFD9245F2EAD

\***************************************************************************/

#region Using directives
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.ComponentModel;
#endregion

namespace UltraDMD
{
    public class AnimationImageList
    {
        private Int32 _fps;
        public string[] _aImages;
        public bool _loop;

        public AnimationImageList()
        {
            _fps = 5;
            _aImages = null;
        }
        public void SetFramesPerSecond(Int32 fps)
        {
            if (fps > 0 && fps <= 30)
            {
                _fps = fps;
            }
            else if (fps > 30)
            {
                _fps = 30;
            }
            else
            {
                _fps = 5;
            }
        }
        public Int32 GetFramesPerSecond()
        {
            return _fps;
        }
    }

    public class Video
    {
        private Int32 _videoStretchMode;
        private bool _loop;
        private string _videoFilename;

        public Video(Int32 mode, bool loop, string videoFilename)
        {
            _videoStretchMode = (mode%4);
            _loop = loop;
            _videoFilename = videoFilename;
        }
        public Int32 GetVideoStretchMode()
        {
            return _videoStretchMode;
        }
        public bool IsLoop()
        {
            return _loop;
        }
        public string GetVideoFilename()
        {
            return _videoFilename;
        }
    }

    public abstract class DisplayScene
    {
        //returns true when done
        public virtual void DrawVideoFrame() { }
        public virtual void StopVideo() { }
        public abstract bool Show();
        public virtual bool IsAnimated() { return false;}
        public virtual void SetBackgroundObj(ref Object obj) { }
        public virtual void SetId(string id) { }
        public virtual string GetId() {return "";}
        public virtual void Modify(string textTop, string textBottom) { }
        public virtual void ModifyWaitTime(int pauseTime) { }
    }



    public class DMDQueue
    {
        private System.Collections.Queue _queue0;
        private System.Threading.AutoResetEvent _autoEvent;
        private System.Threading.Thread _processingThread;
        private long _quit;
        private long _cancelCurrent;
        private long _sceneStarted;
        private string _cancelById;
        private DisplayScene _currentScene;

        public DMDQueue()
        {
            _quit = 0;
            _cancelCurrent = 0;
            _cancelById = null;
            _currentScene = null;
            _sceneStarted = 0;
            _queue0 = new System.Collections.Queue();
            _autoEvent = new System.Threading.AutoResetEvent(false);

            _processingThread = new System.Threading.Thread(new System.Threading.ThreadStart(ProcessQueue));
            _processingThread.Start();
        }
        public void Enqueue(ref DisplayScene d)
        {
            lock (_queue0)
            {
                _queue0.Enqueue(d);
            }
            _autoEvent.Set();
        }
        public void ClearQueue()
        {
            lock (_queue0)
            {
                while (_queue0.Count > 0)
                {
                    _queue0.Dequeue();
                }
                //_queue0.Clear();
            }
        }
        public void CancelCurrent()
        {
            System.Threading.Interlocked.Exchange(ref _cancelCurrent, (long)1);
        }

        public void CancelCurrent(string id)
        {
            lock (_queue0)
            {
                if (_queue0.Count > 0)
                {
                    DisplayScene d = (DisplayScene)_queue0.Peek();
                    if (d.GetId() != null && d.GetId().Equals(id))
                    {
                        d.ModifyWaitTime(0);
                        _queue0.Dequeue();
                    }
                }
                _cancelById = id;
            }
        }

        public void Modify(string id, string textTop, string textBottom)
        {
            lock (_queue0)
            {
                if (_currentScene != null && _currentScene.GetId() != null && _currentScene.GetId().Equals(id))
                {
                    _currentScene.Modify(textTop, textBottom);
                }
            }
        }

        public void Modify(string id, string textTop, string textBottom, Int32 pauseTime)
        {
            lock (_queue0)
            {
                if (_currentScene != null && _currentScene.GetId() != null && _currentScene.GetId().Equals(id))
                {
                    _currentScene.Modify(textTop, textBottom);
                    _currentScene.ModifyWaitTime(pauseTime);
                }
            }
        }

        public bool IsSceneAnimating()
        {
            int count = 0;
            lock (_queue0)
            {
                count = _queue0.Count;
            }
            return (count > 0 || 0 != System.Threading.Interlocked.Read(ref _sceneStarted));
        }

        public void Exit()
        {
            System.Threading.Interlocked.Exchange(ref _quit, 1);
        }

        public void ProcessQueue()
        {
            while (0 == System.Threading.Interlocked.Read(ref _quit))
            {
                //Wait for an event to be queued
                _autoEvent.WaitOne();

                int count = 1;
                //continue processing the queue until it is empty
                while(count > 0)
                {
                    lock (_queue0)
                    {
                        count = _queue0.Count;
                    }

                    if (count > 0)
                    {
                        DisplayScene d = null;

                        lock (_queue0)
                        {
                            d = (DisplayScene)_queue0.Dequeue();
                            _currentScene = d;
                        }
                        System.Threading.Interlocked.Exchange(ref _sceneStarted, 1);
                        System.Threading.Interlocked.Exchange(ref _cancelCurrent, 0);
 
                        while (0 ==  System.Threading.Interlocked.Read(ref _cancelCurrent))
                        {
                            lock (_queue0)
                            {
                                if (_cancelById != null && _cancelById.Length > 0 && d.GetId() != null && d.GetId().Equals(_cancelById))
                                {
                                    _cancelById = null;
                                    break;
                                }
                            }
                            d.DrawVideoFrame();
                            if (Program.draw.SurfaceFontTransitionState == XDMD.Device.PlayState.Ready)
                            {
                                if (d.Show())
                                {
                                    break;
                                }
                            }
                            Program.draw.RenderWait();

                            if( 1 == System.Threading.Interlocked.Read(ref _quit))
                            {
                                System.Threading.Interlocked.Exchange(ref _sceneStarted, 0);
                                return;
                            }
                        }//while not cancelCurrent
                        if (0 != System.Threading.Interlocked.Read(ref _cancelCurrent))
                        {
                            d.ModifyWaitTime(0);
                        }
                        d.StopVideo();
                        System.Threading.Interlocked.Exchange(ref _sceneStarted, 0);
                        lock (_queue0)
                        {
                            _cancelById = null;
                            _currentScene = null;
                        }
                    }//if count > 0
                }//while true
            }//while not quit
            System.Diagnostics.Trace.WriteLine("DMDScene thread exiting");
        }
    }

    [ClassInterface(ClassInterfaceType.None)]           // No ClassInterface
    [ComSourceInterfaces(typeof(IDMDObjectEvents))]
    [Guid(DMDObject.ClassId), ComVisible(true)]
    public class DMDObject : ReferenceCountedObject, IDMDObject
    {
        #region COM Component Registration

        internal const string ClassId =
            "E1612654-304A-4E07-A236-EB64D6D4F511";
        internal const string InterfaceId =
            "F7E68187-251F-4DFB-AF79-F1D4D69EE188";
        internal const string EventsId =
            "0DECFF48-5492-43E7-AB6C-BFD9245F2EAD";

        /* This now comes from the assembly's file version
        internal const Int32 majorVersion = 1;
        internal const Int32 minorVersion = 2;
        internal const Int32 buildNumber = 150930;
        */
        
        // These routines perform the additional COM registration needed by 
        // the service.

        [EditorBrowsable(EditorBrowsableState.Never)]
        [ComRegisterFunction()]
        public static void Register(Type t)
        {
            try
            {
                COMHelper.RegasmRegisterLocalServer(t);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); // Log the error
                throw ex; // Re-throw the exception
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [ComUnregisterFunction()]
        public static void Unregister(Type t)
        {
            try
            {
                COMHelper.RegasmUnregisterLocalServer(t);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); // Log the error
                throw ex; // Re-throw the exception
            }
        }

        #endregion

        #region Properties

        private float fField = 0;
        private string _basePath;
        private DMDQueue _queue;
        private XDMD.Surface _scoreboardBackground;
        private Int32 _scoreboardSelectedBrightness;
        private Int32 _scoreboardUnselectedBrightness;
        private bool _bScoresUseComma;
        private Int32 _videoStretchMode;

        private System.Collections.ArrayList _objectRegistrationCache;

        public float FloatProperty
        {
            get { return this.fField; }
            set
            {
                bool cancel = false;
                // Raise the event FloatPropertyChanging
                if (null != FloatPropertyChanging)
                    FloatPropertyChanging(value, ref cancel);
                if (!cancel)
                    this.fField = value;
            }
        }

        #endregion

        #region Methods

        public void GetProcessThreadID(out uint processId, out uint threadId)
        {
            processId = NativeMethod.GetCurrentProcessId();
            threadId = NativeMethod.GetCurrentThreadId();
        }

        public void Init()
        {
            _bScoresUseComma = false;
            _queue = new DMDQueue();
            DisplayVersionInfo();
            _scoreboardSelectedBrightness = 15;
            _scoreboardUnselectedBrightness = 10;
            _objectRegistrationCache = new System.Collections.ArrayList();
            _videoStretchMode = 0;
        }

        public void Uninit()
        {
            CancelRendering();
            Clear();
        }

        public Int32 GetMajorVersion()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            return fvi.FileMajorPart;
        }

        public Int32 GetMinorVersion()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            return fvi.FileMinorPart;
        }

        public Int32 GetBuildNumber()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            return fvi.FileBuildPart * 10000 + fvi.FilePrivatePart; 
        }

        public bool SetVisibleVirtualDMD(bool bVisible)
        {
            bool bRtn = Program.draw.Visible;
            Program.draw.Visible = bVisible;
            return bRtn;
        }

        public bool SetFlipY(bool bFlip)
        {
            bool bRtn = Program.draw.FlipY;
            Program.draw.FlipY = bFlip;
            return bRtn;
        }

        public bool IsRendering()
        {
            return (_queue.IsSceneAnimating());
        }

        public void CancelRendering()
        {
            _queue.ClearQueue();
            if (_queue.IsSceneAnimating())
            {
                _queue.CancelCurrent();
            }
        }

        public void CancelRenderingWithId(string id)
        {
            _queue.CancelCurrent(id);
        }

        public void Clear()
        {
            Program.draw.Clear();
            Program.draw.Render();
        }

        public void SetProjectFolder(string basePath)
        {
            _basePath = basePath;
        }

        public void DisplayScoresWithComma(bool bComma)
        {
        }

        public void SetVideoStretchMode(Int32 mode)//stretch, crop to top, crop to center, crop to bottom
        {
            _videoStretchMode = (mode%4);
        }

        //returns an identifier which may be cast to a string and passed as a background.  The imageList is a space separated list of png (or other) files
        public Int32 CreateAnimationFromImages(Int32 fps, bool loop, string imagelist)
        {
            AnimationImageList imageList = new AnimationImageList();
            imageList.SetFramesPerSecond(fps);
            imageList._loop = loop;

            string[] aImages = imagelist.Split(new Char[] { ',' });
            imageList._aImages = new string[aImages.Length];
            Int32 iImage = 0;
            foreach(string strImg in aImages)
            {
                imageList._aImages[iImage++] = System.IO.Path.Combine(_basePath, strImg);
            }
            return _objectRegistrationCache.Add(imageList)+1000;
        }

        public Int32 RegisterVideo(Int32 mode, bool loop, string videoFilename)
        {
            Video v = new Video(mode, loop, System.IO.Path.Combine(_basePath, videoFilename));
            return _objectRegistrationCache.Add(v) + 1000;
        }


        public void DisplayVersionInfo()
        {
            DisplayScene s = new DisplaySplashScreen(GetMajorVersion(), GetMinorVersion(), GetBuildNumber());
            _queue.Enqueue(ref s);
        }

        public void DisplayScene00(string background, string toptext, Int32 topBrightness, string bottomtext, Int32 bottomBrightness, Int32 animateIn, Int32 pauseTime, Int32 animateOut)
        {
            Object backgroundObj = null;
            string backgroundFullpath = null;
            if (background != null && background.Length > 0)
            {
                Int32 bkgnd = 0;
                if (background[0] >= '1' && background[0] <= '9')
                {
                    bkgnd = Convert.ToInt32(background);
                }
                if (bkgnd >= 1000 && bkgnd < (_objectRegistrationCache.Count + 1000))
                {
                    backgroundObj = _objectRegistrationCache[bkgnd - 1000];
                }
                else
                {
                    backgroundFullpath = System.IO.Path.Combine(_basePath, background);
                }
            }

            DisplayScene s = null;
            if (toptext == null || toptext.Length == 0)
            {
                s = new DisplaySceneSingleLine(backgroundFullpath, bottomtext, (bottomBrightness%16), (XDMD.Device.AnimationType)(animateIn % ((int)XDMD.Device.AnimationType.None + 1)), pauseTime, (XDMD.Device.AnimationType)(animateOut % ((int)XDMD.Device.AnimationType.None + 1)), _videoStretchMode);
            }
            else if (bottomtext == null || bottomtext.Length == 0)
            {
                s = new DisplaySceneSingleLine(backgroundFullpath, toptext, topBrightness%16, (XDMD.Device.AnimationType)(animateIn % ((int)XDMD.Device.AnimationType.None + 1)), pauseTime, (XDMD.Device.AnimationType)(animateOut % ((int)XDMD.Device.AnimationType.None + 1)), _videoStretchMode);
            }
            else
            {
                s = new DisplaySceneTwoLines(backgroundFullpath, toptext, topBrightness%16, bottomtext, bottomBrightness%16, (XDMD.Device.AnimationType)(animateIn % ((int)XDMD.Device.AnimationType.None + 1)), pauseTime, (XDMD.Device.AnimationType)(animateOut % ((int)XDMD.Device.AnimationType.None + 1)), _videoStretchMode);
            }

            if (backgroundObj != null)
            {
                s.SetBackgroundObj(ref backgroundObj);
            }

            if (animateIn == (Int32)XDMD.Device.AnimationType.None && pauseTime == 0 && animateOut == (Int32)XDMD.Device.AnimationType.None
                && _queue.IsSceneAnimating() == false && s.IsAnimated() == false)
            {
                s.Show();
                Program.draw.RenderWait();
            }
            else
            {
                lock (_queue)
                {
                    _queue.Enqueue(ref s);
                }
            }
        }

        public void DisplayScene00Ex(string background, string toptext, Int32 topBrightness, Int32 topOutlineBrightness, string bottomtext, Int32 bottomBrightness, Int32 bottomOutlineBrightness, Int32 animateIn, Int32 pauseTime, Int32 animateOut)
        {
            DisplayScene00ExWithId("", false, background, toptext, topBrightness, topOutlineBrightness, bottomtext, bottomBrightness, bottomOutlineBrightness, animateIn, pauseTime, animateOut); 
        }

        public void DisplayScene00ExWithId(string id, bool cancelPrev, string background, string toptext, Int32 topBrightness, Int32 topOutlineBrightness, string bottomtext, Int32 bottomBrightness, Int32 bottomOutlineBrightness, Int32 animateIn, Int32 pauseTime, Int32 animateOut)
        {
            if (cancelPrev)
            {
                _queue.CancelCurrent(id);
            }

            Object backgroundObj = null;

            string backgroundFullpath = null;
            if (background != null && background.Length > 0)
            {
                Int32 bkgnd = 0;
                if (background[0] >= '1' && background[0] <= '9')
                {
                    bkgnd = Convert.ToInt32(background);
                }
                if (bkgnd >= 1000 && bkgnd < (_objectRegistrationCache.Count + 1000))
                {
                    backgroundObj = _objectRegistrationCache[bkgnd - 1000];
                }
                else
                {
                    backgroundFullpath = System.IO.Path.Combine(_basePath, background);
                }
            }

            DisplayScene s = null;
            if (toptext == null || toptext.Length == 0)
            {
                s = new DisplaySceneOutlineSingleLine(backgroundFullpath, bottomtext, bottomBrightness, bottomOutlineBrightness, (XDMD.Device.AnimationType)(animateIn % ((int)XDMD.Device.AnimationType.None + 1)), pauseTime, (XDMD.Device.AnimationType)(animateOut % ((int)XDMD.Device.AnimationType.None + 1)), _videoStretchMode);
            }
            else if (bottomtext == null || bottomtext.Length == 0)
            {
                s = new DisplaySceneOutlineSingleLine(backgroundFullpath, toptext, topBrightness, topOutlineBrightness, (XDMD.Device.AnimationType)(animateIn % ((int)XDMD.Device.AnimationType.None + 1)), pauseTime, (XDMD.Device.AnimationType)(animateOut % ((int)XDMD.Device.AnimationType.None + 1)), _videoStretchMode);
            }
            else
            {
                s = new DisplaySceneOutlineTwoLines(backgroundFullpath, toptext, topBrightness, topOutlineBrightness, bottomtext, bottomBrightness, bottomOutlineBrightness, (XDMD.Device.AnimationType)(animateIn % ((int)XDMD.Device.AnimationType.None + 1)), pauseTime, (XDMD.Device.AnimationType)(animateOut % ((int)XDMD.Device.AnimationType.None + 1)), _videoStretchMode);
            }

            if (backgroundObj != null)
            {
                s.SetBackgroundObj(ref backgroundObj);
            }

            s.SetId(id);
            lock (_queue)
            {
                _queue.Enqueue(ref s);
            }
        }

        public void DisplayScene01(string sceneId, string background, string toptext, Int32 topBrightness, Int32 topOutlineBrightness, Int32 animateIn, Int32 pauseTime, Int32 animateOut)
        {
            Object backgroundObj = null;
            string backgroundFullpath = null;
            if (background != null && background.Length > 0)
            {
                Int32 bkgnd = 0;
                if (background[0] >= '1' && background[0] <= '9')
                {
                    bkgnd = Convert.ToInt32(background);
                }
                if (bkgnd >= 1000 && bkgnd < (_objectRegistrationCache.Count + 1000))
                {
                    backgroundObj = _objectRegistrationCache[bkgnd - 1000];
                }
                else
                {
                    backgroundFullpath = System.IO.Path.Combine(_basePath, background);
                }
            }

            DisplayScene s = null;
            if (toptext == null || toptext.Length == 0)
            {
                toptext = " ";
            }
            s = new DisplaySceneScrollingLine(backgroundFullpath, toptext, topBrightness, topOutlineBrightness, (XDMD.Device.AnimationType)(animateIn % ((int)XDMD.Device.AnimationType.None + 1)), pauseTime, (XDMD.Device.AnimationType)(animateOut % ((int)XDMD.Device.AnimationType.None + 1)), _videoStretchMode);

            if (backgroundObj != null)
            {
                s.SetBackgroundObj(ref backgroundObj);
            }

            if (animateIn == (Int32)XDMD.Device.AnimationType.None && animateOut == (Int32)XDMD.Device.AnimationType.None
                && _queue.IsSceneAnimating() == false && s.IsAnimated() == false)
            {
                if (pauseTime == 0)
                {
                    s.Show();
                    Program.draw.RenderWait();
                }
                else
                {
                    s.SetId(sceneId);
                    lock (_queue)
                    {
                        _queue.Enqueue(ref s);
                    }
                    s.Show();
                    Program.draw.Render();
                }
            }
            else
            {
                lock (_queue)
                {
                    _queue.Enqueue(ref s);
                }
            }
        }

        public void SetScoreboardBackgroundImage(string filename, Int32 selectedBrightness, Int32 unselectedBrightness)
        {
            _scoreboardSelectedBrightness = selectedBrightness;
            _scoreboardUnselectedBrightness = unselectedBrightness;

            if (_scoreboardBackground != null)
            {
                _scoreboardBackground.Dispose();
                _scoreboardBackground = null;
            }
            try
            {
                _scoreboardBackground = new XDMD.Surface(System.IO.Path.Combine(_basePath, filename), Program.draw);
            }
            catch(Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.ToString());
            }
        }


        public void DisplayScoreboard(Int32 cPlayers, Int32 highlightedPlayer, Int32 cScore1, Int32 cScore2, Int32 cScore3, Int32 cScore4, string lowerLeft, string lowerRight)
        {
            //Don't interrupt the current animation
            if (_queue.IsSceneAnimating())
            {
                return;
            }
            string score1 = (cPlayers < 1 ? "" : (_bScoresUseComma ? cScore1.ToString("#,##0") : cScore1.ToString()));
            string score2 = (cPlayers < 2 ? "" : (_bScoresUseComma ? cScore2.ToString("#,##0") : cScore2.ToString()));
            string score3 = (cPlayers < 3 ? "" : (_bScoresUseComma ? cScore3.ToString("#,##0") : cScore3.ToString()));
            string score4 = (cPlayers < 4 ? "" : (_bScoresUseComma ? cScore4.ToString("#,##0") : cScore4.ToString()));

            XDMD.Font fontSmall = new XDMD.Font(Program.GetImageByName("f4by5"), Program.draw);
            XDMD.Font fontNormal = new XDMD.Font(Program.GetImageByName("f5by7"), Program.draw);
            XDMD.Font fontHighlight = new XDMD.Font(Program.GetImageByName("f6by12"), Program.draw);
            
            Program.draw.Clear();

            if (_scoreboardBackground != null)
            {
                _scoreboardBackground.draw(0, 0);
            }

            if (highlightedPlayer == 1)
            {
                fontHighlight.Draw(1, 1, _scoreboardSelectedBrightness, score1);
            }
            else
            {
                fontNormal.Draw(1, 1, _scoreboardUnselectedBrightness, score1);
            }

            if (highlightedPlayer == 2)
            {
                Size s = fontHighlight.MeasureString(score2);
                fontHighlight.Draw(127 - s.Width, 1, _scoreboardSelectedBrightness, score2);
            }
            else
            {
                Size s = fontNormal.MeasureString(score2);
                fontNormal.Draw((127 - s.Width), 1, _scoreboardUnselectedBrightness, score2);
            }

            if (highlightedPlayer == 3)
            {
                fontHighlight.Draw(1, 11, _scoreboardSelectedBrightness, score3);
            }
            else
            {
                fontNormal.Draw(1, 15, _scoreboardUnselectedBrightness, score3);
            }

            if (highlightedPlayer == 4)
            {
                Size s = fontHighlight.MeasureString(score4);
                fontHighlight.Draw(127 - s.Width, 11, _scoreboardSelectedBrightness, score4);
            }
            else
            {
                Size s = fontNormal.MeasureString(score4);
                fontNormal.Draw(127 - s.Width, 15, _scoreboardUnselectedBrightness, score4);
            }

            if (lowerLeft != null)
            {
                fontSmall.Draw(1, 26, _scoreboardUnselectedBrightness, lowerLeft);
            }
            if (lowerRight != null)
            {
                Size s = fontSmall.MeasureString(lowerRight);
                fontSmall.Draw(127 - s.Width, 26, _scoreboardUnselectedBrightness, lowerRight);
            }
            Program.draw.Render();
        }

        protected void DrawContent_NoOutline(string _text, Int32 _textBrightness)
        {
            XDMD.Font _fontNormal = null;
            Size _sizeTop;

            _fontNormal = new XDMD.Font(Program.GetImageByName("f14by26"), Program.draw);
            _sizeTop = _fontNormal.MeasureString(_text);

            if (_sizeTop.Width > 120)
            {
                _fontNormal = new XDMD.Font(Program.GetImageByName("f12by24"), Program.draw);
                _sizeTop = _fontNormal.MeasureString(_text);

                if (_sizeTop.Width > 120)
                {
                    _fontNormal = new XDMD.Font(Program.GetImageByName("f7by13"), Program.draw);
                    _sizeTop = _fontNormal.MeasureString(_text);
                }
            }

            _fontNormal.Draw((128 - _sizeTop.Width) / 2, (32 - _sizeTop.Height) / 2, (_textBrightness < 0 ? 15 : (_textBrightness % 16)), _text);
        }
        protected void DrawContent_NoFill(string _text, Int32 _textOutlineBrightness)
        {
            XDMD.Font _fontNormalOutline = null;
            Size _sizeTop;

            _fontNormalOutline = new XDMD.Font(Program.GetImageByName("f14by26_outline"), Program.draw);
            _sizeTop = _fontNormalOutline.MeasureString(_text);

            if (_sizeTop.Width > 120)
            {
                _fontNormalOutline = new XDMD.Font(Program.GetImageByName("f12by24_outline"), Program.draw);
                _sizeTop = _fontNormalOutline.MeasureString(_text);

                if (_sizeTop.Width > 120)
                {
                    _fontNormalOutline = new XDMD.Font(Program.GetImageByName("f7by13_outline"), Program.draw);
                    _sizeTop = _fontNormalOutline.MeasureString(_text);
                }
            }

            _fontNormalOutline.Draw((128 - _sizeTop.Width) / 2, (32 - _sizeTop.Height) / 2, (_textOutlineBrightness < 0 ? 15 : (_textOutlineBrightness % 16)), _text);
        }
        protected void DrawContent_OutlineAndFill(string _text, Int32 _textBrightness, Int32 _textOutlineBrightness)
        {
            XDMD.Font _fontNormal = null;
            XDMD.Font _fontNormalOutline = null;
            Size _sizeTop;

            _fontNormal = new XDMD.Font(Program.GetImageByName("f14by26_fill"), Program.draw);
            _fontNormalOutline = new XDMD.Font(Program.GetImageByName("f14by26_outline"), Program.draw);
            _sizeTop = _fontNormalOutline.MeasureString(_text);

            if (_sizeTop.Width > 120)
            {
                _fontNormal = new XDMD.Font(Program.GetImageByName("f12by24_fill"), Program.draw);
                _fontNormalOutline = new XDMD.Font(Program.GetImageByName("f12by24_outline"), Program.draw);
                _sizeTop = _fontNormalOutline.MeasureString(_text);

                if (_sizeTop.Width > 120)
                {
                    _fontNormal = new XDMD.Font(Program.GetImageByName("f7by13_fill"), Program.draw);
                    _fontNormalOutline = new XDMD.Font(Program.GetImageByName("f7by13_outline"), Program.draw);
                    _sizeTop = _fontNormalOutline.MeasureString(_text);
                }
            }

            _fontNormalOutline.Draw((128 - _sizeTop.Width) / 2, (32 - _sizeTop.Height) / 2, (_textOutlineBrightness % 16), _text);
            _fontNormal.Draw((128 - _sizeTop.Width) / 2, (32 - _sizeTop.Height) / 2, (_textBrightness % 16), _text);
        }

        public void ModifyScene00(string id, string toptext, string bottomtext)
        {
            _queue.Modify(id, toptext, bottomtext);
        }

        public void ModifyScene00Ex(string id, string toptext, string bottomtext, int pauseTime)
        {
            _queue.Modify(id, toptext, bottomtext, pauseTime);
        }


        public void DisplayText(string text, Int32 textBrightness, Int32 textOutlineBrightness)
        {
            if (textBrightness != -1 && textOutlineBrightness != -1)
            {
                DrawContent_OutlineAndFill(text, textBrightness, textOutlineBrightness);
            }
            else if (textBrightness == -1)
            {
                DrawContent_NoFill(text, textOutlineBrightness);
            }
            else
            {
                DrawContent_NoOutline(text, textBrightness);
            }
        }

        public void ScrollingCredits(string background, string text, Int32 textBrightness, Int32 animateIn, Int32 pauseTime, Int32 animateOut)
        {
            Object backgroundObj = null;
            string backgroundFullpath = null;
            if (background != null && background.Length > 0)
            {
                Int32 bkgnd = 0;
                if (background[0] >= '1' && background[0] <= '9')
                {
                    bkgnd = Convert.ToInt32(background);
                }
                if (bkgnd >= 1000 && bkgnd < (_objectRegistrationCache.Count + 1000))
                {
                    backgroundObj = _objectRegistrationCache[bkgnd - 1000];
                }
                else
                {
                    backgroundFullpath = System.IO.Path.Combine(_basePath, background);
                }
            }

            string[] txt = text.Split(new Char[] { '\n','|' });
            DisplayScene s = new DisplaySceneScrollingCredits(backgroundFullpath, txt, textBrightness, (XDMD.Device.AnimationType)animateIn, pauseTime, (XDMD.Device.AnimationType)animateOut, _videoStretchMode);

            if (backgroundObj != null)
            {
                s.SetBackgroundObj(ref backgroundObj);
            }

            if (animateIn == (Int32)XDMD.Device.AnimationType.None && pauseTime == 0 && animateOut == (Int32)XDMD.Device.AnimationType.None
                && _queue.IsSceneAnimating() == false && s.IsAnimated() == false)
            {
                s.Show();
                Program.draw.RenderWait();
            }
            else
            {
                lock (_queue)
                {
                    _queue.Enqueue(ref s);
                }
            }

        }


        #endregion

        #region Events

        [ComVisible(false)]
        public delegate void FloatPropertyChangingEventHandler(float NewValue, ref bool Cancel);
        public event FloatPropertyChangingEventHandler FloatPropertyChanging;

        #endregion
    }

    /// <summary>
    /// Class factory for the class DMDObject.
    /// </summary>
    internal class DMDObjectClassFactory : IClassFactory
    {
        public int CreateInstance(IntPtr pUnkOuter, ref Guid riid, 
            out IntPtr ppvObject)
        {
            ppvObject = IntPtr.Zero;

            if (pUnkOuter != IntPtr.Zero)
            {
                // The pUnkOuter parameter was non-NULL and the object does 
                // not support aggregation.
                Marshal.ThrowExceptionForHR(COMNative.CLASS_E_NOAGGREGATION);
            }

            if (riid == new Guid(DMDObject.ClassId) ||
                riid == new Guid(COMNative.IID_IDispatch) ||
                riid == new Guid(COMNative.IID_IUnknown))
            {
                // Create the instance of the .NET object
                ppvObject = Marshal.GetComInterfaceForObject(
                    new DMDObject(), typeof(IDMDObject));
            }
            else
            {
                // The object that ppvObject points to does not support the 
                // interface identified by riid.
                Marshal.ThrowExceptionForHR(COMNative.E_NOINTERFACE);
            }

            return 0;   // S_OK
        }

        public int LockServer(bool fLock)
        {
            return 0;   // S_OK
        }
    }

    /// <summary>
    /// Reference counted object base.
    /// </summary>
    [ComVisible(false)]
    public class ReferenceCountedObject
    {
        public ReferenceCountedObject()
        {
            // Increment the lock count of objects in the COM server.
            ExeCOMServer.Instance.Lock();
        }

        ~ReferenceCountedObject()
        {
            // Decrement the lock count of objects in the COM server.
            ExeCOMServer.Instance.Unlock();
        }
    }
}
