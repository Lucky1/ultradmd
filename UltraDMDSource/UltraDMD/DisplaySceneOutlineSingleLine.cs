﻿#region Using directives
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.ComponentModel;
#endregion

namespace UltraDMD
{
    public class DisplaySceneOutlineSingleLine : DisplaySceneSingleLine
    {
        protected Int32 _textOutlineBrightness;
        protected XDMD.Font _fontNormalOutline;

        public DisplaySceneOutlineSingleLine(string background, string text, Int32 textBrightness, Int32 textOutlineBrightness
            , XDMD.Device.AnimationType animateIn, Int32 pauseTime, XDMD.Device.AnimationType animateOut, Int32 videoStretchMode) :
            base(background, text, textBrightness, animateIn, pauseTime, animateOut, videoStretchMode)
        {
            _textOutlineBrightness = textOutlineBrightness;
            _fontNormalOutline = null;
        }
        protected void DrawContent_NoOutline()
        {
            if (_fontNormal == null)
            {
                //string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                //_fontNormal = new XDMD.Font(exePath + "\\Fonts\\f14by26.gif", Program.draw);
                _fontNormal = new XDMD.Font(Program.GetImageByName("f14by26"), Program.draw);
                _sizeTop = _fontNormal.MeasureString(_text);

                if (_sizeTop.Width > 120)
                {
                    //_fontNormal = new XDMD.Font(exePath + "\\Fonts\\f12by24.gif", Program.draw);
                    _fontNormal = new XDMD.Font(Program.GetImageByName("f12by24"), Program.draw);
                    _sizeTop = _fontNormal.MeasureString(_text);

                    if (_sizeTop.Width > 120)
                    {
                        //_fontNormal = new XDMD.Font(exePath + "\\Fonts\\f7by13.gif", Program.draw);
                        _fontNormal = new XDMD.Font(Program.GetImageByName("f7by13"), Program.draw);
                        _sizeTop = _fontNormal.MeasureString(_text);
                    }
                }
            }

            _fontNormal.Draw(_surfaceContent, (128 - _sizeTop.Width) / 2, (32 - _sizeTop.Height) / 2, (_textBrightness < 0 ? 15 : (_textBrightness % 16)), _text, Program.color);
        }
        protected void DrawContent_NoFill()
        {
            if (_fontNormalOutline == null)
            {
                //string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                //_fontNormalOutline = new XDMD.Font(exePath + "\\Fonts\\f14by26.outline.gif", Program.draw);
                _fontNormalOutline = new XDMD.Font(Program.GetImageByName("f14by26_outline"), Program.draw);
                _sizeTop = _fontNormalOutline.MeasureString(_text);

                if (_sizeTop.Width > 120)
                {
                    //_fontNormalOutline = new XDMD.Font(exePath + "\\Fonts\\f12by24.outline.gif", Program.draw);
                    _fontNormalOutline = new XDMD.Font(Program.GetImageByName("f12by24_outline"), Program.draw);
                    _sizeTop = _fontNormalOutline.MeasureString(_text);

                    if (_sizeTop.Width > 120)
                    {
                        //_fontNormalOutline = new XDMD.Font(exePath + "\\Fonts\\f7by13.outline.gif", Program.draw);
                        _fontNormalOutline = new XDMD.Font(Program.GetImageByName("f7by13_outline"), Program.draw);
                        _sizeTop = _fontNormalOutline.MeasureString(_text);
                    }
                }
            }
            _fontNormalOutline.Draw(_surfaceContent, (128 - _sizeTop.Width) / 2, (32 - _sizeTop.Height) / 2, (_textOutlineBrightness < 0 ? 15 : (_textOutlineBrightness % 16)), _text, Program.color);
        }
        protected void DrawContent_OutlineAndFill()
        {
            if (_fontNormal == null)
            {
                //string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                //_fontNormal = new XDMD.Font(exePath + "\\Fonts\\f14by26.fill.gif", Program.draw);
                _fontNormal = new XDMD.Font(Program.GetImageByName("f14by26_fill"), Program.draw);
                //_fontNormalOutline = new XDMD.Font(exePath + "\\Fonts\\f14by26.outline.gif", Program.draw);
                _fontNormalOutline = new XDMD.Font(Program.GetImageByName("f14by26_outline"), Program.draw);
                _sizeTop = _fontNormalOutline.MeasureString(_text);

                if (_sizeTop.Width > 120)
                {
                    //_fontNormal = new XDMD.Font(exePath + "\\Fonts\\f12by24.fill.gif", Program.draw);
                    _fontNormal = new XDMD.Font(Program.GetImageByName("f12by24_fill"), Program.draw);
                    //_fontNormalOutline = new XDMD.Font(exePath + "\\Fonts\\f12by24.outline.gif", Program.draw);
                    _fontNormalOutline = new XDMD.Font(Program.GetImageByName("f12by24_outline"), Program.draw);
                    _sizeTop = _fontNormalOutline.MeasureString(_text);

                    if (_sizeTop.Width > 120)
                    {
                        //_fontNormal = new XDMD.Font(exePath + "\\Fonts\\f7by13.fill.gif", Program.draw);
                        _fontNormal = new XDMD.Font(Program.GetImageByName("f7by13_fill"), Program.draw);
                        //_fontNormalOutline = new XDMD.Font(exePath + "\\Fonts\\f7by13.outline.gif", Program.draw);
                        _fontNormalOutline = new XDMD.Font(Program.GetImageByName("f7by13_outline"), Program.draw);
                        _sizeTop = _fontNormalOutline.MeasureString(_text);
                    }
                }
            }
            _fontNormalOutline.Draw(_surfaceContent, (128 - _sizeTop.Width) / 2, (32 - _sizeTop.Height) / 2, (_textOutlineBrightness % 16), _text, Program.color);
            _fontNormal.Draw(_surfaceContent, (128 - _sizeTop.Width) / 2, (32 - _sizeTop.Height) / 2, (_textBrightness % 16), _text, Program.color);
        }

        protected override void DrawContent()
        {
            if (_textOutlineBrightness < 0)
            {   //-1 indicates NO outline
                DrawContent_NoOutline();
            }
            else if (_textBrightness < 0)
            {   //-1 indicates DON'T fill (outline only - we'll ignore the error case of BOTH values being -1)
                DrawContent_NoFill();
            }
            else
            {   //outline AND fill
                DrawContent_OutlineAndFill();
            }

            _surfaceContent.draw(_surface, 0, 0);
        }
    }
}
