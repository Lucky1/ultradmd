﻿namespace UltraDMD
{
    partial class FormConfigureUltraDMD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.linkLabelXDMD = new System.Windows.Forms.LinkLabel();
            this.linkLabelSpesoft = new System.Windows.Forms.LinkLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.listBoxDMDColor = new System.Windows.Forms.ListBox();
            this.listBoxDMDSize = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.linkLabelUltraDMD = new System.Windows.Forms.LinkLabel();
            this.buttonExit = new System.Windows.Forms.Button();
            this.checkBoxHideVDMD = new System.Windows.Forms.CheckBox();
            this.buttonDisplayErrorLog = new System.Windows.Forms.Button();
            this.buttonClearErrorLog = new System.Windows.Forms.Button();
            this.checkBoxFullColor = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(167, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Copyright 0000 Stephen Rakonza";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 2;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(180, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "DMD Color";
            // 
            // linkLabelXDMD
            // 
            this.linkLabelXDMD.AutoSize = true;
            this.linkLabelXDMD.Location = new System.Drawing.Point(140, 13);
            this.linkLabelXDMD.Name = "linkLabelXDMD";
            this.linkLabelXDMD.Size = new System.Drawing.Size(83, 13);
            this.linkLabelXDMD.TabIndex = 7;
            this.linkLabelXDMD.TabStop = true;
            this.linkLabelXDMD.Text = "http://xdmd.info";
            this.linkLabelXDMD.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelXDMD_LinkClicked);
            // 
            // linkLabelSpesoft
            // 
            this.linkLabelSpesoft.AutoSize = true;
            this.linkLabelSpesoft.Location = new System.Drawing.Point(101, 26);
            this.linkLabelSpesoft.Name = "linkLabelSpesoft";
            this.linkLabelSpesoft.Size = new System.Drawing.Size(122, 13);
            this.linkLabelSpesoft.TabIndex = 8;
            this.linkLabelSpesoft.TabStop = true;
            this.linkLabelSpesoft.Text = "http://www.spesoft.com";
            this.linkLabelSpesoft.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelSpesoft_LinkClicked);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.linkLabelXDMD);
            this.groupBox1.Controls.Add(this.linkLabelSpesoft);
            this.groupBox1.Location = new System.Drawing.Point(350, 409);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(230, 53);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Powered by XDMD";
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(465, 36);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(109, 23);
            this.buttonOK.TabIndex = 10;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(465, 65);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(109, 23);
            this.buttonCancel.TabIndex = 11;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.button2_Click);
            // 
            // listBoxDMDColor
            // 
            this.listBoxDMDColor.FormattingEnabled = true;
            this.listBoxDMDColor.Location = new System.Drawing.Point(183, 52);
            this.listBoxDMDColor.Name = "listBoxDMDColor";
            this.listBoxDMDColor.Size = new System.Drawing.Size(150, 225);
            this.listBoxDMDColor.TabIndex = 12;
            this.listBoxDMDColor.SelectedIndexChanged += new System.EventHandler(this.listBoxDMDColor_SelectedIndexChanged);
            // 
            // listBoxDMDSize
            // 
            this.listBoxDMDSize.FormattingEnabled = true;
            this.listBoxDMDSize.Location = new System.Drawing.Point(15, 52);
            this.listBoxDMDSize.Name = "listBoxDMDSize";
            this.listBoxDMDSize.Size = new System.Drawing.Size(150, 225);
            this.listBoxDMDSize.TabIndex = 14;
            this.listBoxDMDSize.SelectedIndexChanged += new System.EventHandler(this.listBoxDMDSize_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "DMD Size";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(349, 36);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(63, 17);
            this.checkBox1.TabIndex = 15;
            this.checkBox1.Text = "Invert X";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(349, 59);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(63, 17);
            this.checkBox2.TabIndex = 16;
            this.checkBox2.Text = "Invert Y";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.linkLabelUltraDMD);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(15, 409);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(215, 53);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "UltraDMD version 0.0.000000";
            // 
            // linkLabelUltraDMD
            // 
            this.linkLabelUltraDMD.AutoSize = true;
            this.linkLabelUltraDMD.Location = new System.Drawing.Point(6, 16);
            this.linkLabelUltraDMD.Name = "linkLabelUltraDMD";
            this.linkLabelUltraDMD.Size = new System.Drawing.Size(152, 13);
            this.linkLabelUltraDMD.TabIndex = 8;
            this.linkLabelUltraDMD.TabStop = true;
            this.linkLabelUltraDMD.Text = "http://ultradmd.wordpress.com";
            this.linkLabelUltraDMD.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelUltraDMD_LinkClicked);
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(465, 241);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(109, 23);
            this.buttonExit.TabIndex = 18;
            this.buttonExit.Text = "Exit UltraDMD";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // checkBoxHideVDMD
            // 
            this.checkBoxHideVDMD.AutoSize = true;
            this.checkBoxHideVDMD.Location = new System.Drawing.Point(15, 17);
            this.checkBoxHideVDMD.Name = "checkBoxHideVDMD";
            this.checkBoxHideVDMD.Size = new System.Drawing.Size(108, 17);
            this.checkBoxHideVDMD.TabIndex = 19;
            this.checkBoxHideVDMD.Text = "Hide Virtual DMD";
            this.checkBoxHideVDMD.UseVisualStyleBackColor = true;
            this.checkBoxHideVDMD.CheckedChanged += new System.EventHandler(this.checkBoxHideVDMD_CheckedChanged);
            // 
            // buttonDisplayErrorLog
            // 
            this.buttonDisplayErrorLog.Location = new System.Drawing.Point(465, 139);
            this.buttonDisplayErrorLog.Name = "buttonDisplayErrorLog";
            this.buttonDisplayErrorLog.Size = new System.Drawing.Size(109, 23);
            this.buttonDisplayErrorLog.TabIndex = 20;
            this.buttonDisplayErrorLog.Text = "Display Error Log";
            this.buttonDisplayErrorLog.UseVisualStyleBackColor = true;
            this.buttonDisplayErrorLog.Click += new System.EventHandler(this.buttonDisplayErrorLog_Click);
            // 
            // buttonClearErrorLog
            // 
            this.buttonClearErrorLog.Location = new System.Drawing.Point(465, 168);
            this.buttonClearErrorLog.Name = "buttonClearErrorLog";
            this.buttonClearErrorLog.Size = new System.Drawing.Size(109, 23);
            this.buttonClearErrorLog.TabIndex = 21;
            this.buttonClearErrorLog.Text = "Clear Error Log";
            this.buttonClearErrorLog.UseVisualStyleBackColor = true;
            this.buttonClearErrorLog.Click += new System.EventHandler(this.buttonClearErrorLog_Click);
            // 
            // checkBoxFullColor
            // 
            this.checkBoxFullColor.AutoSize = true;
            this.checkBoxFullColor.Location = new System.Drawing.Point(183, 16);
            this.checkBoxFullColor.Name = "checkBoxFullColor";
            this.checkBoxFullColor.Size = new System.Drawing.Size(91, 17);
            this.checkBoxFullColor.TabIndex = 22;
            this.checkBoxFullColor.Text = "Use Full Color";
            this.checkBoxFullColor.UseVisualStyleBackColor = true;
            this.checkBoxFullColor.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // FormConfigureUltraDMD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 476);
            this.Controls.Add(this.checkBoxFullColor);
            this.Controls.Add(this.buttonClearErrorLog);
            this.Controls.Add(this.buttonDisplayErrorLog);
            this.Controls.Add(this.checkBoxHideVDMD);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.listBoxDMDSize);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.listBoxDMDColor);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormConfigureUltraDMD";
            this.Text = "Configure UltraDMD";
            this.Load += new System.EventHandler(this.FormConfigureUltraDMD_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.LinkLabel linkLabelXDMD;
        private System.Windows.Forms.LinkLabel linkLabelSpesoft;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.ListBox listBoxDMDColor;
        private System.Windows.Forms.ListBox listBoxDMDSize;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.CheckBox checkBoxHideVDMD;
        private System.Windows.Forms.Button buttonDisplayErrorLog;
        private System.Windows.Forms.Button buttonClearErrorLog;
        private System.Windows.Forms.LinkLabel linkLabelUltraDMD;
        private System.Windows.Forms.CheckBox checkBoxFullColor;
    }
}