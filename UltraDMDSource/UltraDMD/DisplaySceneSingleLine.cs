﻿#region Using directives
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.ComponentModel;
#endregion

namespace UltraDMD
{
    public class DisplaySceneSingleLine : DisplayScene
    {
        protected Object _backgroundObj;
        protected Image _animatedGif;
        protected int _animagedGifFrameCount;
        protected int _gifLoopCount;
        protected string _background;
        protected string _text;
        protected Int32 _pauseTime;
        protected XDMD.Device.AnimationType _animateIn;
        protected XDMD.Device.AnimationType _animateOut;
        protected string _id;
        protected int _idTransition;

        protected Int32 _step;
        protected XDMD.Surface _surface;
        protected XDMD.Surface _surfaceContent;
        protected bool _bVideoSurface;

        protected XDMD.Font _fontNormal;
        protected Int32 _textBrightness;
        protected Size _sizeTop;
        protected Int32 _endTime;
        protected Int32 _videoStretchMode;
        protected System.Collections.ArrayList _listImages;
        protected Int32 _tickCountNextFrame;
        protected XDMD.Surface _surfaceBkgnd;
        protected Int32 _yContentOffset;
        protected double _wContentScale;

        public DisplaySceneSingleLine(string background, string text, Int32 textBrightness, XDMD.Device.AnimationType animateIn, Int32 pauseTime, XDMD.Device.AnimationType animateOut, Int32 videoStretchMode)
        {
            _backgroundObj = null;
            _animatedGif = null;
            _animagedGifFrameCount = 0;
            _background = background;
            _animateIn = animateIn;
            _animateOut = animateOut;
            _pauseTime = pauseTime;
            _step = 0;
            _text = text;

            _surface = null;
            _bVideoSurface = false;
            _endTime = 0;
            _fontNormal = null;
            _videoStretchMode = videoStretchMode;
            _textBrightness = textBrightness;
            _surfaceBkgnd = null;
            _yContentOffset = 0;
            _wContentScale = 1.0;
        }

        public override void SetId(string id)
        {
            _id = id;
        }
        public override string GetId()
        {
            return _id;
        }

        public override void Modify(string textTop, string textBottom)
        {
            _text = textTop;
        }

        public override void ModifyWaitTime(int pauseTime)
        {
            _endTime = System.Environment.TickCount + _pauseTime;
            Program.draw.ModifyTransitionWaitTime(_idTransition, pauseTime * 10 / (24 * 7));
        }

        public override void SetBackgroundObj(ref Object obj)
        {
            _backgroundObj = obj;
        }

        public override bool IsAnimated()
        {
            return _bVideoSurface || (_backgroundObj != null);
        }

        protected virtual void DrawContent()
        {
            if (_fontNormal == null)
            {
                //string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                //_fontNormal = new XDMD.Font(exePath + "\\Fonts\\f14by26.gif", Program.draw);
                _fontNormal = new XDMD.Font(Program.GetImageByName("f14by26"), Program.draw);
                _sizeTop = _fontNormal.MeasureString(_text);
                if (_sizeTop.Width > 120)
                {
                    //_fontNormal = new XDMD.Font(exePath + "\\Fonts\\f12by24.gif", Program.draw);
                    _fontNormal = new XDMD.Font(Program.GetImageByName("f12by24"), Program.draw);
                    _sizeTop = _fontNormal.MeasureString(_text);

                    if (_sizeTop.Width > 120)
                    {
                        //_fontNormal = new XDMD.Font(exePath + "\\Fonts\\f7by13.gif", Program.draw);
                        _fontNormal = new XDMD.Font(Program.GetImageByName("f7by13"), Program.draw);
                        _sizeTop = _fontNormal.MeasureString(_text);
                    }
                }
            }

            _fontNormal.Draw(_surfaceContent, (128 - _sizeTop.Width) / 2, (32 - _sizeTop.Height) / 2, _textBrightness, _text, Program.color);
        }

        public override void DrawVideoFrame()
        {
            if (_bVideoSurface && _surface != null)
            {
                Program.draw.DrawVideoFrame(_surfaceBkgnd, new Rectangle(0, 0, _surfaceBkgnd.Width, _surfaceBkgnd.Height));
                _surfaceBkgnd.draw(_surface, 0, _yContentOffset, _wContentScale);
            }
            else if (_backgroundObj != null && _backgroundObj.GetType()==typeof(AnimationImageList) && _surface != null)
            {
                AnimationImageList animationImageList = (AnimationImageList)_backgroundObj;
                if (_tickCountNextFrame < System.Environment.TickCount)
                {
                    ++_step;
                    _tickCountNextFrame = Environment.TickCount + (int)((1.0 / animationImageList.GetFramesPerSecond()) * 1000);
                }
                if (animationImageList._loop == false && _step >= _listImages.Count)
                {
                    _step = _listImages.Count - 1;
                }
                XDMD.Surface s = (XDMD.Surface)(_listImages[_step % _listImages.Count]);

                s.draw(_surfaceContent, 0, 0);
            }
            else if (_animatedGif != null)
            {
                try
                {
                    if (_tickCountNextFrame < System.Environment.TickCount)
                    {
                        ++_step;

                        System.Drawing.Imaging.PropertyItem item = _animatedGif.GetPropertyItem(0x5100); // FrameDelay in libgdiplus
                        // Time is in 1/100th of a second
                        int delay = (item.Value[0] + item.Value[1] * 256) * 10;
                        _tickCountNextFrame = Environment.TickCount + delay;
                    }
                    if (false/*_gifLoopCount<2*/ && _step >= _animagedGifFrameCount)
                    {
                        _step = _animagedGifFrameCount - 1;
                    }
                    _animatedGif.SelectActiveFrame(System.Drawing.Imaging.FrameDimension.Time, (_step % _animagedGifFrameCount));
                    _surfaceBkgnd = new XDMD.Surface(new Bitmap(_animatedGif), Program.draw);
                    _surface.Clear();
                    _surfaceContent.Clear();
                    _surfaceBkgnd.draw(_surfaceContent, new Rectangle((int)((128.0 - _animatedGif.Width) / 2.0), _yContentOffset, _animatedGif.Width, _animatedGif.Height), _wContentScale);
                }
                catch
                {
                }
            }
            else if (_surfaceBkgnd != null)
            {
                _surface.Clear();
                _surfaceContent.Clear();
                _surfaceBkgnd.draw(_surfaceContent, 0, 0);
            }
            else
            {
                return;
            }

            DrawContent();
            _surfaceContent.draw(_surface, 0, 0);
        }

        public override void StopVideo()
        {
            if (_bVideoSurface && _surface != null)
            {
                Program.draw.StopVideo();
            }
        }

        public override bool Show()
        {
            if (_step > 0)
            {
                if (System.Environment.TickCount > _endTime)
                {
                    return true;
                }

                return false;
            }
            ++_step;

            try
            {
                _tickCountNextFrame = 0;
                if (_backgroundObj != null && _backgroundObj.GetType() == typeof(AnimationImageList))
                {
                        AnimationImageList animationImageList = (AnimationImageList)_backgroundObj;

                        _surfaceContent = new XDMD.Surface(128, 32, Program.draw);
                        _surface = new XDMD.Surface(128, 32, Program.draw);
                        _listImages = new System.Collections.ArrayList();

                        foreach (string strImg in animationImageList._aImages)
                        {
                            _listImages.Add(new XDMD.Surface(strImg, Program.draw));
                        }
                }
                else if (_backgroundObj != null && _backgroundObj.GetType() == typeof(Video))
                {
                    Video v = (Video)_backgroundObj;
                    _videoStretchMode = v.GetVideoStretchMode();
                    _surfaceContent = new XDMD.Surface(128, 32, Program.draw);
                    _surface = new XDMD.Surface(128, 32, Program.draw);
                    if (System.IO.Path.GetExtension(v.GetVideoFilename()).ToLower().Equals(".wmv") || System.IO.Path.GetExtension(v.GetVideoFilename()).ToLower().Equals(".mp4"))
                    {
                        _bVideoSurface = true;
                        Program.draw.PlayVideo(v.GetVideoFilename(), true, false, new Rectangle(0, 0, 128, 32), _videoStretchMode > 0);
                        _surfaceBkgnd = new XDMD.Surface(128, (_videoStretchMode == 0 ? 32 : (int)((128.0 / Program.draw.VideoWidth) * (Program.draw.VideoHeight))), Program.draw);
                    }
                }
                else if (_background != null && _background.Length > 0)
                {
                    _surfaceContent = new XDMD.Surface(128, 32, Program.draw);
                    _surface = new XDMD.Surface(128, 32, Program.draw);
                    if (!System.IO.File.Exists(_background))
                    {
                        Bitmap bmpBkgnd = Program.GetImageByName("error00");
                        _surfaceBkgnd = new XDMD.Surface(bmpBkgnd, Program.draw);

                        using (System.IO.StreamWriter sw = System.IO.File.AppendText(System.IO.Path.Combine(System.IO.Path.GetTempPath(), "UltraDMD.log")))
                        {
                            sw.WriteLine(DateTime.Now + " file not found: " + _background);
                        }
                        _background = null;
                    }
                    else if (System.IO.Path.GetExtension(_background).ToLower().Equals(".wmv") || System.IO.Path.GetExtension(_background).ToLower().Equals(".mp4"))
                    {
                        _bVideoSurface = true;
                        Program.draw.PlayVideo(_background, true, false, new Rectangle(0, 0, 128, 32), _videoStretchMode > 0);
                        _surfaceBkgnd = new XDMD.Surface(128, (int)((128.0 / Program.draw.VideoWidth) * (Program.draw.VideoHeight)), Program.draw);
                    }
                    else if (System.IO.Path.GetExtension(_background).ToLower().Equals(".gif"))
                    {
                        _animatedGif = Image.FromFile(_background);
                        System.Drawing.Imaging.PropertyItem item = _animatedGif.GetPropertyItem(0x5101);
                        _gifLoopCount = (item.Value[0] + item.Value[1] * 256);

                        System.Drawing.Imaging.FrameDimension dimension = new System.Drawing.Imaging.FrameDimension(_animatedGif.FrameDimensionsList[0]); //gets the GUID
                        _animagedGifFrameCount = _animatedGif.GetFrameCount(dimension); //total frames in the animation
                        _surfaceBkgnd = new XDMD.Surface(_animatedGif.Width, _animatedGif.Height, Program.draw);
                        _wContentScale = 128.0 / (_animatedGif.Width);
                    }
                    else
                    {
                        _surfaceBkgnd = new XDMD.Surface(_background, Program.draw);
                    }
                }
                else
                {
                    Bitmap bmpBkgnd = Program.GetImageByName("border00");
                    _surfaceBkgnd = new XDMD.Surface(bmpBkgnd, Program.draw);
                    _surfaceContent = new XDMD.Surface(128, 32, Program.draw);
                    _surface = new XDMD.Surface(128, 32, Program.draw);
                }

                _endTime = System.Environment.TickCount + _pauseTime;

                if (_id == null)
                {
                    Program.draw.Clear();
                }

                Int32 h = 32;
                Int32 y = 0;
                if (_surfaceBkgnd != null)
                {
                    h = (_videoStretchMode == 0 ? 32 : _surfaceBkgnd.Height);
                    if (_videoStretchMode == 2)
                    {
                        y = (32 - h) / 2;
                    }
                    else if (_videoStretchMode == 3)
                    {
                        y = 32 - h;
                    }
                }
                _yContentOffset = y;
                DrawVideoFrame();

                _idTransition = Program.draw.TransitionSurface(_surface, new Rectangle(0, 0, 128, 32), _animateIn, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_08, _pauseTime * 10 / (24 * 7), _animateOut, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_08);
            }
            catch (Exception ex)
            {
                throw ex; // Re-throw the exception
            }
            return false;
        }
    }
}
