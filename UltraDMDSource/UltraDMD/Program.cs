﻿/****************************** Module Header ******************************\
* Module Name:	Program.cs
* Project:		UltraDMD
* Copyright (c) Stephen Rakonza.
* 
* The main entry point for the application. It is responsible for starting  
* the out-of-proc COM server registered in the exectuable.
* 
\***************************************************************************/

#region Using directives
using System;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
#endregion


namespace UltraDMD
{
    public class SubclassHWND : NativeWindow
    {
        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        public static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

        [DllImport("user32.dll")]
        public static extern bool ClientToScreen(IntPtr hWnd, out Point pt);

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        const Int32 WM_MOUSEMOVE = 0x0200;
        const Int32 MK_LBUTTON = 0x0001;

        const Int32 WM_LBUTTONDOWN = 0x0201;
        const Int32 WM_LBUTTONUP = 0x0202;
        const Int32 WM_RBUTTONUP = 0x0205;
        const Int32 WM_CLOSE = 0x0010;

        const short SWP_NOMOVE = 0X2;
        const short SWP_NOSIZE = 1;
        const short SWP_NOZORDER = 0x0004;

        Point pt;
        Point ptWindow;

        public SubclassHWND()
        {
            pt.X = -1;
            pt.Y = -1;
        }
        public void Configure()
        {
            NotTopMost();
            FormConfigureUltraDMD f = new FormConfigureUltraDMD();
            f.Show();
        }
        static int GetInt(IntPtr ptr)
        {
            return IntPtr.Size == 8 ? unchecked((int)ptr.ToInt64()) : ptr.ToInt32();
        }
        public static Point GetPoint(IntPtr lParam)
        {
            return new Point(GetInt(lParam));
        }

        public void TopMost()
        {
            SetWindowPos(this.Handle, -1, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
        }

        public void NotTopMost()
        {
            SetWindowPos(this.Handle, -2, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_LBUTTONDOWN:
                    {
                        TopMost();

                        Point _pt = GetPoint(m.LParam);
                        ClientToScreen(m.HWnd, out _pt);
                        pt.X = _pt.X;
                        pt.Y = _pt.Y;

                        RECT rect = new RECT();
                        if (GetWindowRect(m.HWnd, ref rect))
                        {
                            ptWindow.X = rect.Left;
                            ptWindow.Y = rect.Top;
                        }
                        else
                        {
                            pt.X = -1;
                            pt.Y = -1;
                        }
                        break;
                    }
                case WM_LBUTTONUP:
                    {
                        pt.X = -1;
                        pt.Y = -1;

                        Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\UltraDMD", true);
                        RECT rect = new RECT();
                        if (GetWindowRect(m.HWnd, ref rect))
                        {
                            key.SetValue("x", rect.Left);
                            key.SetValue("y", rect.Top);
                        }
                        break;
                    }
                case WM_MOUSEMOVE:
                    if (pt.X != -1)
                    {
                        Point ptCur = GetPoint(m.LParam);
                        ClientToScreen(m.HWnd, out ptCur);
                        SetWindowPos(m.HWnd, 0, ptWindow.X + ptCur.X - pt.X, ptWindow.Y + ptCur.Y - pt.Y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
                    }
                    break;
                case WM_RBUTTONUP:
                    {
                        Configure();
                        break;
                    }
                case WM_CLOSE:
                    {
                        Application.Exit();
                        System.Diagnostics.Process.GetCurrentProcess().Kill();
                        break;
                    }
            }
            base.WndProc(ref m);
        }
    }

    class Program
    {
        static public XDMD.Device draw;
        static public SubclassHWND s;
        static public int hDMD;
        static public bool bFullColor = false;
        static public Color color;
        static private bool bConfig;

        static public void subclass(IntPtr hwnd)
        {
            s = new SubclassHWND();
            s.AssignHandle(hwnd);
            s.TopMost();

            if (bConfig)
            {
                s.Configure();                
            }
        }

        public static bool IsAdministrator()
        {
            return (new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()))
                    .IsInRole(System.Security.Principal.WindowsBuiltInRole.Administrator);
        } 
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            s = null;

            bool help = false;
            bool install = false;
            bool uninstall = false;
            bConfig = false;

            if (args != null)
            {
                for (int iArg = 0; iArg < args.Length; ++iArg)
                {
                    string arg = args[iArg];

                    char ch = arg[0];
                    if (ch == '-' || ch == '/')
                    { //option
                        string option = arg.Substring(1);
                        if (option.StartsWith("c") || option.StartsWith("C"))
                        {
                            //Configure
                            bConfig = true;
                        }
                        else if (option.StartsWith("?") || option.StartsWith("h") || option.StartsWith("H"))
                        {
                            help = true;
                        }
                        else if (option.StartsWith("i") || option.StartsWith("I"))
                        {
                            install = true;
                        }
                        else if(option.StartsWith("u") || option.StartsWith("U"))
                        {
                            uninstall = true;
                        }
                    }
                }
            }

            if (install || uninstall)
            {
                if (!IsAdministrator())
                {
                    System.Windows.Forms.MessageBox.Show("Administrator privilege require to install or uninstall.  To open a command prompt with Administrator priviledges: Start Menu, type 'cmd', right-click on 'cmd.exe', select 'Run as administrator'.");
                    return;
                }

                // Get the location of regasm
                string regasmPath = System.Runtime.InteropServices.RuntimeEnvironment.GetRuntimeDirectory() + @"regasm.exe";
                // Get the location of our EXE
                string exePath = System.Reflection.Assembly.GetEntryAssembly().Location;// System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                string cmd = exePath;
                if (uninstall)
                {
                    cmd = "/u " + exePath;
                }

                // Execute regasm
                System.Diagnostics.Process.Start(regasmPath, cmd);
                System.Diagnostics.Debug.WriteLine(regasmPath);
                System.Diagnostics.Debug.WriteLine(exePath);
                System.Threading.Thread.Sleep(5000);
                return;
            }

            if (help)
            {
                System.Diagnostics.Process.Start("http://ultradmd.wordpress.com");
            }
            else
            {
                //Before we run the COM server, store its location in the registry so it only has to be run once.
                int x = 0;
                int y = 0;
                int w = 512;// +128;
                int h = 128;// +32;
                bool bHideVDMD = false;
                bool bFlipY = false;
                string strColor = "OrangeRed";
                Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\UltraDMD");
                if (key != null)
                {
                    x = (key.GetValue("x") == null ? 0 : Convert.ToInt32(key.GetValue("x").ToString()));
                    y = (key.GetValue("y") == null ? 0 : Convert.ToInt32(key.GetValue("y").ToString()));
                    w = (key.GetValue("w") == null ? 512 : Convert.ToInt32(key.GetValue("w").ToString()));
                    h = (key.GetValue("h") == null ? 128 : Convert.ToInt32(key.GetValue("h").ToString()));
                    strColor = (key.GetValue("color") == null ? "OrangeRed" : key.GetValue("color").ToString());
                    bHideVDMD = (key.GetValue("hideVDMD") == null ? false : Convert.ToBoolean(key.GetValue("hideVDMD")));
                    bFullColor = (key.GetValue("fullColor") == null ? false : Convert.ToBoolean(key.GetValue("fullColor")));
                    bFlipY = (key.GetValue("flipY") == null ? false : Convert.ToBoolean(key.GetValue("flipY")));
                }
                else
                {
                    Microsoft.Win32.RegistryKey keySoftware = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software", true);
                    keySoftware.CreateSubKey("UltraDMD");
                }
                string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                Environment.SetEnvironmentVariable("PATH", exePath + ";" + Environment.GetEnvironmentVariable("PATH"));

                string xdmdPath = System.IO.Path.Combine(exePath, "XDMD.dll");
                System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(xdmdPath);

                //First check the XDMD file version.  It needs to be at least 1.1
                bool bCompatibleVersionOfXDMD = (fvi.FileMajorPart >= 1 && fvi.FileMinorPart >= 1);

                if(!bCompatibleVersionOfXDMD)
                {
                    System.Windows.Forms.MessageBox.Show("UltraDMD requires a newer version of XDMD.  Please visit http://xdmd.info or http://ultradmd.wordpress.com", "UltraDMD: Install Problem");
                    return;
                }

                // Run the out-of-process COM server
                if (bHideVDMD && !bConfig)
                {
                    draw = new XDMD.Device(false, true, bFullColor);
                }
                else
                {
                    draw = new XDMD.Device(x, y, w, h, true, bFullColor);
                }

                try
                {
                    color = Color.FromName(strColor);
                    
                    draw.Color = color;
                    draw.FlipY = bFlipY;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine(ex.ToString());
                }
                hDMD = h;

                ExeCOMServer.Instance.Run();

                if (draw != null)
                {
                    draw.Dispose();
                }
            }
        }

        public static Bitmap GetImageByName(string imageName)
        {
            System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
            string resourceName = asm.GetName().Name + ".Properties.Resources";
            var rm = new System.Resources.ResourceManager(resourceName, asm);
            return (Bitmap)rm.GetObject(imageName);

        }
    }
}
