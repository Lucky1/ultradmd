﻿#region Using directives
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.ComponentModel;
#endregion

namespace UltraDMD
{
    public class DisplaySceneTwoLines : DisplaySceneSingleLine
    {
        protected string _bottomtext;

        protected Int32 _bottomBrightness;
        protected XDMD.Font _fontBottom;
        protected Size _sizeBottom;

        public DisplaySceneTwoLines(string background, string toptext, Int32 topBrightness, string bottomtext, Int32 bottomBrightness, XDMD.Device.AnimationType animateIn, Int32 pauseTime, XDMD.Device.AnimationType animateOut, Int32 videoStretchMode) :
            base(background, toptext, topBrightness, animateIn, pauseTime, animateOut, videoStretchMode)
        {
            _bottomtext = bottomtext;
            _fontBottom = null;
            _bottomBrightness = bottomBrightness;
        }

        public override void Modify(string textTop, string textBottom)
        {
            _text = textTop;
            _bottomtext = textBottom;
        }

        protected override void DrawContent()
        {
            if (_fontNormal == null)
            {
                //string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                //_fontNormal = new XDMD.Font(exePath + "\\Fonts\\f5by7.gif", Program.draw);
                _fontNormal = new XDMD.Font(Program.GetImageByName("f5by7"), Program.draw);
                //_fontBottom = new XDMD.Font(exePath + "\\Fonts\\f6by12.gif", Program.draw);
                _fontBottom = new XDMD.Font(Program.GetImageByName("f6by12"), Program.draw);

                _sizeTop = _fontNormal.MeasureString(_text);
                _sizeBottom = _fontBottom.MeasureString(_bottomtext);
            }
            if (_bVideoSurface)
            {
                _fontNormal.Draw(_surfaceContent, (128 - _sizeTop.Width) / 2, 4, _textBrightness, _text, Program.color);
                _fontBottom.Draw(_surfaceContent, (128 - _sizeBottom.Width) / 2, 15, _bottomBrightness, _bottomtext, Program.color);
            }
            else
            {
                _fontNormal.Draw(_surfaceContent, (128 - _sizeTop.Width) / 2, 4, 13, _text, Program.color);
                _fontBottom.Draw(_surfaceContent, (128 - _sizeBottom.Width) / 2, 15, 15, _bottomtext, Program.color);
            }
        }
    }
}
